const path = require('path');

module.exports = {
  components: 'src/components/**/[A-Z]*.js',
  require: [
    path.join(__dirname, '/src/styles/global.scss')
  ],
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/styleguide/Wrapper.js')
  }
};
