import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { setDefaultBreakpoints, BreakpointProvider } from 'react-socks';
import Cookies from 'js-cookie';

import red from '@material-ui/core/colors/red';
import { withStyles } from '@material-ui/core/styles';
import io from 'socket.io-client';

import { GLOBALS, CONFIG } from './constants';
import store from './redux';
import { setLang } from './redux/actions/locale.actions';
import { userLogin, userLogout } from './redux/actions/user.actions';
import { hideGlobalSnack } from './redux/actions/snack.actions';
import { isSupportedLanguage, getLanguageMessageList } from './locales/locale.messages';
import { refreshUser } from './services/userServices';
import SocketContext from './socket';

import Loading from './components/Loading';
import sitemap from './routing/siteMap';
import NotFound from './components/__pages/NotFound';
import Header from './components/__subpages/Header';
import Footer from './components/__subpages/Footer';
import Login, { ResetPasswordSubmit } from './components/__pages/Login';
import ResetPassword from './components/__pages/Login/ResetPassword';
import VeifyEmail from './components/__pages/Login/VerifyEmail';
import Home from './components/__pages/Home';
import Gallery from './components/__pages/Gallery';
import Studios from './components/__pages/Studios';
import Feed from './components/__pages/Feed';
import Artist from './components/__pages/Artist';
import ArtWork from './components/__pages/ArtWork';
import EditArtwork from './components/__pages/User/EditArtwork';
import User from './components/__pages/User';
import GlobalDialog from './components/GlobalDialog';
import InitiateFollows from './components/__pages/User/InitiateFollows';
import Search from './components/__pages/Search';
import Wrapper from './Wrapper';
import FooterNavigation from './components/FooterNavigation';
import { Exhibition, Exhibitions } from './components/__pages/Exhibition';

import './styles/global.scss';
import './styles/theStyles.scss';

setDefaultBreakpoints([
  { s: 0 },
  { m: 992 }
]);

const snackStyles = {
  error: {
    background: red[500]
  }
};

const mapStateToProps = state => (
  {
    lang: state.locale.lang,
    snack: state.snack,
    user: state.user
  }
);

class App extends Component {
  constructor() {
    super();

    this.state = {
      isUserRefreshInProgress: true,
      socketConnected: false
    };

    this.hideFooterOnPages = [
      '/login',
      '/verify',
      '/resetpassword',
      /user\/*/g,
      /exhibition\/*/g
    ];

    this.hideMobileFooter = [
      /exhibitions\/*/g
    ];

    this.socket = undefined;

    const navigatorLanguage = (
      navigator.language
      || navigator.browserLanguage
      || GLOBALS.DEFAULT_SITE_LANGUAGE
    )
      .substring(0, 2);

    let lang = '';

    const languageCookie = Cookies.get(CONFIG.LANG_COOKIE_NAME);

    if (!languageCookie) {
      lang = isSupportedLanguage(navigatorLanguage)
        ? navigatorLanguage
        : GLOBALS.DEFAULT_SITE_LANGUAGE;
    } else {
      lang = languageCookie;
    }

    store.dispatch(setLang(lang));
  }

  componentDidMount() {
    refreshUser(
      () => {
        this.setState({ isUserRefreshInProgress: true });
      },
      (user, token) => {
        store.dispatch(userLogin(user, token));
        this.setState({ isUserRefreshInProgress: false });
        this.connect();
      },
      (e) => {
        console.error(e);
        store.dispatch(userLogout());
        this.setState({ isUserRefreshInProgress: false });
      }
    );
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;
    if ((user && !prevProps.user)) {
      this.connect();
    }
  }

  connect = () => {
    // let error = null;
    const { user } = this.props;
    this.socket = io(CONFIG.SOCKET_URL, {
      secure: true,
      rejectUnauthorized: false,
      path: '/live/socket.io'
    });
    this.socket.on('connect', () => {
      // re render App
      this.setState({
        socketConnected: true
      });
      this.socket.emit('userlogin', {
        id: user.id
      });
    });
  }

  closeSnack = () => {
    store.dispatch(hideGlobalSnack());
  };

  render() {
    const { lang, snack, classes, user } = this.props;
    const { isUserRefreshInProgress, socketConnected } = this.state;
    let appLoading = true;
    if (!user) {
      appLoading = false;
    } else if (user && socketConnected) {
      appLoading = false;
    }

    return (
      <IntlProvider locale={lang} messages={getLanguageMessageList(lang)}>
        <BrowserRouter>
          <BreakpointProvider>
            <CssBaseline/>
            <SocketContext.Provider value={this.socket}>
              {
                appLoading || isUserRefreshInProgress
                  ? (
                    <div className="loader">
                      <Loading color="red"/>
                    </div>
                  )
                  : (
                    <Wrapper hideFooterOnPages={this.hideFooterOnPages} hideMobileFooter={this.hideMobileFooter}>
                      <Snackbar
                        anchorOrigin={{
                          vertical: 'top',
                          horizontal: 'center'
                        }}
                        open={snack.message !== ''}
                        autoHideDuration={snack.duration}
                        onClose={this.closeSnack}
                      >
                        <SnackbarContent
                          className={snack.type === 'error' ? classes.error : ''}
                          message={
                            <span id="message-id">{snack.message}</span>
                          }
                          action={[
                            <IconButton
                              key="close"
                              aria-label="Close"
                              color="inherit"
                              onClick={this.closeSnack}
                            >
                              <CloseIcon/>
                            </IconButton>
                          ]}
                        />
                      </Snackbar>
                      <GlobalDialog/>
                      {
                        user && user.initiateFollows === 'false' && <InitiateFollows/>
                      }
                      <Header isUserRefreshInProgress={isUserRefreshInProgress}/>
                      <div id="contentWrapper">
                        <Switch>
                          <Route exact path={sitemap.home} component={Home}/>
                          <Route path={sitemap.gallery} component={Gallery}/>
                          <Route exact path={sitemap.studios} component={Studios}/>
                          <Route exact path={sitemap.feed} component={Feed}/>
                          <Route path={sitemap.artist} component={Artist}/>
                          <Route path={sitemap.user} render={props => <User {...props} isUserRefreshInProgress={isUserRefreshInProgress}/>}/>
                          <Route exact path={sitemap.login} component={Login}/>
                          <Route exact path={sitemap.resetPassword} component={ResetPassword}/>
                          <Route exact path={sitemap.resetPasswordSubmit} component={ResetPasswordSubmit}/>
                          <Route exact path={sitemap.verifyEmail} component={VeifyEmail}/>
                          <Route exact path={sitemap.artWork} component={ArtWork}/>
                          <Route exact path={sitemap.artWorkEdit} render={props => <EditArtwork {...props} isUserRefreshInProgress={isUserRefreshInProgress}/>}/>
                          <Route exact path={sitemap.searchPage} component={Search}/>
                          <Route exact path={sitemap.exhibitions} component={Exhibitions}/>
                          <Route exact path={sitemap.exhibition} component={Exhibition}/>
                          <Route exact path={sitemap.notFound} component={NotFound}/>
                          <Redirect to={sitemap.home}/>
                        </Switch>
                      </div>
                      <Footer hideFooter={this.hideFooterOnPages}/>
                      <FooterNavigation/>
                    </Wrapper>
                  )
              }
            </SocketContext.Provider>
          </BreakpointProvider>
        </BrowserRouter>
      </IntlProvider>
    );
  }
}

App.propTypes = {
  lang: PropTypes.string.isRequired,
  snack: PropTypes.objectOf(PropTypes.any).isRequired,
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any)
};

App.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(withStyles(snackStyles)(App));
