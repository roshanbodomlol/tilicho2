import React from 'react';
import { IntlProvider } from 'react-intl';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from '@material-ui/core/styles';

import Theme from '../styles/Theme';

const en = require('../locales/en.json');

const Wrapper = ({ children }) => (
  <MuiThemeProvider theme={Theme}>
    <IntlProvider locale="en" messages={en}>
      {children}
    </IntlProvider>
  </MuiThemeProvider>
);

Wrapper.propTypes = {
  children: PropTypes.node
};

Wrapper.defaultProps = {
  children: null
};

export default Wrapper;
