export const isUserArtist = (role) => {
  return role > 10 && role < 21;
};
