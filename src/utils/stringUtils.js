import renderHTML from 'react-render-html';

export const nl2br = (string) => {
  return string ? renderHTML(string.replace(/(?:\r\n|\r|\n)/g, '<br />')) : '';
};
