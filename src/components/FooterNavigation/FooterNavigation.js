import React from 'react';
import { NavLink } from 'react-router-dom';
import { generatePath } from 'react-router';
import { Breakpoint } from 'react-socks';
import { connect } from 'react-redux';
import { objectOf, any } from 'prop-types';

import sitemap from '../../routing/siteMap';
import Notifications from '../__subpages/Header/DesktopHeader/Notifications';

import FeedIcon from '../../assets/icons/Feed.svg';
import Gallery from '../../assets/icons/Gallery.svg';
import Home from '../../assets/icons/Home.svg';
import Studios from '../../assets/icons/Studios.svg';

import './FooterNavigation.scss';

const mapStateToProps = ({ user }) => ({ user });

const FooterNavigation = ({ user }) => {
  return (
    <Breakpoint s>
      <div id="footer-navigation">
        <div className="nav-icon">
          <NavLink exact to="/">
            <img src={Home} alt=""/>
          </NavLink>
        </div>
        <div className="nav-icon">
          <NavLink to={generatePath(sitemap.gallery, { cat: 'all' })}>
            <img src={Gallery} alt=""/>
          </NavLink>
        </div>
        <div className="nav-icon">
          <NavLink to={sitemap.studios}>
            <img src={Studios} alt=""/>
          </NavLink>
        </div>
        <div className="nav-icon">
          <NavLink to={sitemap.feed}>
            <img src={FeedIcon} alt=""/>
          </NavLink>
        </div>
        {
          user && <Notifications footer/>
        }
      </div>
    </Breakpoint>
  );
};

FooterNavigation.propTypes = {
  user: objectOf(any)
};

FooterNavigation.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(FooterNavigation);
