import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { CONFIG } from '../../constants';
import AdvancedAvatarInline from '../AdvancedAvatarInline';
import ArtistGalleryInline from '../ArtistGalleryInline';
import './artistShowcase.scss';


const ArtistShowcase = ({ artist, fullWidthGallery }) => {
  const showcaseItemClasses = classnames('artistShowcaseItem', {
    'full-width-gallery': fullWidthGallery
  });
  return (
    <div className={showcaseItemClasses}>
      <AdvancedAvatarInline
        imgPath={`${CONFIG.BUCKET_URL}/users/${artist.picture}`}
        followers={artist.follows.length}
        works={artist.products.length}
        name={artist.name}
        artist={artist}
      />
      <ArtistGalleryInline artistId={artist._id}/>
    </div>
  );
};

ArtistShowcase.propTypes = {
  artist: PropTypes.objectOf(PropTypes.any).isRequired,
  fullWidthGallery: PropTypes.bool
};

ArtistShowcase.defaultProps = {
  fullWidthGallery: false
};

export default ArtistShowcase;
