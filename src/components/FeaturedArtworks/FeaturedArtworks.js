import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import HomeSection from '../HomeSection';
import Loading from '../Loading';
import ImageContainer from '../ImageContainer';
import { getFeatured } from '../../services/productServices';
import { getThumbnail } from '../../utils/mediaUtils';
import { CONFIG } from '../../constants';
import siteMap from '../../routing/siteMap';
import './FeaturedArtworks.scss';

class FeaturedArtworks extends Component {
  state = {
    items: [],
    isLoading: false
  };

  componentDidMount() {
    getFeatured()
      .then((items) => {
        this.setState({ items, isLoading: false });
      })
      .catch((error) => {
        console.log('Error occurred while trying to get featured products.', error);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { isLoading, items } = this.state;

    const featuredList = items.map((item) => {
      const thumbnail = getThumbnail(item.image, 'gallery');
      return (
        <div key={`popular-item-${item._id}`} className="featured-art">
          <Link to={generatePath(siteMap.artWork, { slug: item.slug })}>
            <ImageContainer
              imgPath={`${CONFIG.BUCKET_URL}/${item.image.set.slug}/${thumbnail.fileName}`}
              width="100%"
              height={250}
              size="cover"
              borderRadius={5}
            />
          </Link>
          <div className="details">
            <div className="left">
              <div className="name">{item.name}</div>
              <span>by <Link to={generatePath(siteMap.artist, { slug: item.author.slug })}>{item.author.name}</Link></span>
            </div>
            <div className="right">
              {item.priceField}
            </div>
          </div>
        </div>
      );
    });
    return (
      <HomeSection title="Featured Arts" grey fullHeight fullWidth>
        {
          isLoading
            ? <Loading color="red"/>
            : (
              <div className="featured-art-list">
                {featuredList}
              </div>
            )
        }
      </HomeSection>
    );
  }
}

export default FeaturedArtworks;
