import React, { Component } from 'react';
import Slider from 'react-slick';

class ArtWorkSlider extends Component {
  constructor() {
    super();
    this.settings = {
      dots: true,
      infinite: false,
      autoplay: false,
      arrows: true,
      speed: 500,
      fade: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable: false
    };
    this.singleSlideSettings = {
      dots: false,
      infinite: false,
      autoplay: false,
      arrows: false,
      speed: 500,
      fade: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable: false
    };
  }

  render() {
    return (
      <div id="product-slider">
        <h1>asdas</h1>
      </div>
    );
  }
}

export default ArtWorkSlider;
