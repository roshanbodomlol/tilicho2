import React from 'react';
import PropTypes from 'prop-types';

import './Form.scss';

const Form = ({ onSubmit }) => (
  <form onSubmit={onSubmit}/>
);

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default Form;
