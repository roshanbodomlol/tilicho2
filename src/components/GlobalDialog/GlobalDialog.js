import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';

import Button from '../Button';
import store from '../../redux';
import { hideDialog } from '../../redux/actions/dialog.actions';
import './GlobalDialog.scss';

const mapStateToProps = ({ dialog }) => ({
  dialog
});

const dialogClasses = {
  paper: {
    width: '100%',
    padding: 30
  }
};

class GlobalDialog extends Component {
  closeDialog = () => {
    store.dispatch(hideDialog());
  }

  render() {
    const { dialog, classes } = this.props;
    return (
      <Dialog
        open={dialog.confirm !== null}
        onClose={this.closeDialog}
        aria-labelledby="responsive-dialog-title"
        classes={classes}
      >
        <div id="responsive-dialog-title">{dialog.title}</div>
        <div id="responsive-dialog-message">
          {dialog.message}
        </div>
        <div id="responsive-dialog-actions">
          <span role="button" tabIndex="-1" onClick={this.closeDialog} mode="special">
            {dialog.noLabel}
          </span>
          <Button size="auto" onClick={dialog.confirm} mode="primary">
            {dialog.yesLabel}
          </Button>
        </div>
      </Dialog>
    );
  }
}

GlobalDialog.propTypes = {
  dialog: PropTypes.objectOf(PropTypes.any).isRequired,
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(withStyles(dialogClasses)(GlobalDialog));
