import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import ThumbIcon from '@material-ui/icons/ThumbUp';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

import { CONFIG } from '../../constants';
import store from '../../redux';
import { showGlobalSnack } from '../../redux/actions/snack.actions';
import Styles from './ButtonLike.module.scss';

const mapStateToProps = state => ({
  user: state.user
});

class ButtonLike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      like: false
    };
  }

  componentDidMount() {
    const { user, item } = this.props;
    if (user !== null) {
      const userLikes = user && item.likes.find(userId => userId === user.id);
      if (userLikes) {
        this.setLike();
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { user, item } = this.props;
    if (prevProps.user !== user) {
      if (user !== null) {
        const userLikes = user && item.likes.find(userId => userId === user.id);
        if (userLikes) {
          this.setLike();
        }
      }
    }
  }

  setLike = () => {
    this.setState({ like: true });
  }

  handleClick = () => {
    const token = Cookie.get(CONFIG.AUTH_TOKEN);
    const { history } = this.props;
    if (token) {
      this.handleLike();
    } else {
      store.dispatch(showGlobalSnack('normal', 'You need to log in to do that', 3000));
      history.push('/login');
    }
  }

  handleLike = () => {
    const token = Cookie.get(CONFIG.AUTH_TOKEN);
    const { item, refresh } = this.props;
    this.setState(prevState => (
      prevState.like ? { like: false } : { like: true }
    ));
    axios({
      url: `${CONFIG.API_URL}/post/like`,
      method: 'POST',
      data: { post: item._id },
      headers: {
        Authorization: `JWT ${token}`
      }
    })
      .then((response) => {
        if (response.data.status === 'success') {
          refresh();
        } else throw new Error(response.data.message);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { like } = this.state;
    const buttonClasses = classnames({
      [Styles.commonLikeButton]: true,
      [Styles.isLiked]: like
    });
    const iconStyles = {
      color: like ? '#fff' : '#000'
    };
    return (
      <button className={buttonClasses} onClick={this.handleClick}>
        <span>
          <ThumbIcon
            style={iconStyles}
          />
        </span>
        <span>Like</span>
      </button>
    );
  }
}

ButtonLike.propTypes = {
  item: PropTypes.shape({
    likes: PropTypes.arrayOf(PropTypes.any).isRequired
  }).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  refresh: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any)
};

ButtonLike.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(withRouter(ButtonLike));
