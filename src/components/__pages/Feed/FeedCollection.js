import React from 'react';
import PropTypes from 'prop-types';
import BottomScrollListener from 'react-bottom-scroll-listener';

import Loading from '../../Loading';
import FeedItem from '../../FeedItem';

const FeedCollection = ({ posts, refresh, loadMore, loadingMore, total }) => {
  return (
    <>
      <div className="commonFeedContainer">
        {
          posts.map(post => (
            <FeedItem key={`feed-item-${post._id}`} post={post} refresh={refresh}/>
          ))
        }
      </div>
      {
        loadingMore
          ? <Loading color="red"/>
          : (
            <>
              {
                posts.length < total
                  ? <BottomScrollListener onBottom={loadMore} offset={500}/>
                  : <div style={{ textAlign: 'center', padding: '15px 0' }}>No more posts to show</div>
              }
            </>
          )
      }
    </>
  );
};

FeedCollection.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object).isRequired,
  refresh: PropTypes.func.isRequired,
  loadMore: PropTypes.func.isRequired,
  loadingMore: PropTypes.bool.isRequired,
  total: PropTypes.number.isRequired
};

export default FeedCollection;
