import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';

import store from '../../../redux';
import { showGlobalSnack } from '../../../redux/actions/snack.actions';
import CommonButton from '../../Button';
import { CONFIG } from '../../../constants';

class NewFeed extends Component {
  state = {
    post: '',
    submitting: false,
    image: undefined,
    imageFile: undefined
  };

  onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      this.setState({ imageFile: event.target.files[0] });
      const reader = new FileReader();
      reader.onload = (e) => {
        this.setState({ image: e.target.result });
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const token = Cookie.get(CONFIG.AUTH_TOKEN);
    const { post, imageFile } = this.state;
    const { refresh } = this.props;
    if (token) {
      this.setState({ submitting: true });
      const data = new FormData();
      data.append('post', post);
      if (imageFile) data.append('picture', imageFile);
      axios({
        url: `${CONFIG.API_URL}/post/new`,
        method: 'POST',
        data,
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `JWT ${token}`
        }
      })
        .then((response) => {
          if (response.data.status === 'success') {
            this.setState({
              submitting: false,
              post: '',
              image: undefined,
              imageFile: undefined
            });
            refresh();
          } else {
            throw new Error(response.data.message);
          }
        })
        .catch((e) => {
          console.log(e);
          if (e.message === 'File too large') {
            const message = `Attached image must be less than ${CONFIG.POST_ATTACHMENT_MB} MB`;
            store.dispatch(showGlobalSnack('error', message, 3000));
          } else {
            store.dispatch(showGlobalSnack('error', e.message.toString(), 3000));
          }
          this.setState({
            submitting: false,
            image: undefined,
            imageFile: undefined
          });
        });
    }
  }

  render() {
    const { image, post, submitting } = this.state;
    return (
      <div className="commonFeedContainer typeNewFeed">
        <form onSubmit={this.handleSubmit}>
          <div className="content">
            {
              image && (
                <div id="post-picture">
                  <img src={image} alt=""/>
                </div>
              )
            }
            <textarea
              required
              placeholder="Write an Article"
              value={post}
              onChange={e => this.setState({ post: e.target.value })}
            />
          </div>
          <div className="action">
            <label className="add-image" htmlFor="post-picture">
              <input type="file" id="post-picture" className="custom-input-file" onChange={this.onImageChange}/>
              <span>Add a picture</span>
            </label>
            <CommonButton loading={submitting} size="medium" mode="specialBlack" type="submit">Post</CommonButton>
          </div>
        </form>
      </div>
    );
  }
}

NewFeed.propTypes = {
  refresh: PropTypes.func.isRequired
};

export default NewFeed;
