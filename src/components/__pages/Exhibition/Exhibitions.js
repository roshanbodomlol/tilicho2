import React, { Component } from 'react';
import { connect } from 'react-redux';
import { generatePath } from 'react-router';
import Slider from 'react-slick';
import { Link, withRouter } from 'react-router-dom';
import cn from 'classnames';
import Breakpoint from 'react-socks';

import { showDialog, hideDialog } from '../../../redux/actions/dialog.actions';
import { setLoginBack } from '../../../redux/actions/global.actions';
import { nl2br } from '../../../utils/stringUtils';
import { list } from '../../../services/exhibition.services';
import store from '../../../redux';
import { showGlobalSnack } from '../../../redux/actions/snack.actions';
import sitemap from '../../../routing/siteMap';
import Loading from '../../Loading';
import css from './exhibitions.module.scss';
import titleImage from '../../../assets/img/bhitta-title.svg';
import titleImageSmall from '../../../assets/img/bhitta-title-s.svg';

const mapStateToProps = ({ user }) => ({ user });

class Exhibitions extends Component {
  state = {
    loading: true,
    exhibitions: [],
    slideIndex: 0
  };

  slickRef = React.createRef();

  componentDidMount() {
    list()
      .then((exhibitions) => {
        this.setState({ exhibitions, loading: false });
      })
      .catch(e => console.log(e));
  }

  confirmExplicit = (exhibition) => {
    const { dispatch, history } = this.props;
    dispatch(
      showDialog(
        'Explicit Content',
        'This exhibition contains explicit material. By continuing you confirm that you are over 18 years of age.',
        () => {
          history.push(generatePath(sitemap.exhibition, { slug: exhibition._id }));
          dispatch(hideDialog());
        },
        'Continue',
        'Cancel'
      )
    );
  };

  
  render() {
    const { loading, exhibitions, slideIndex } = this.state;
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 2,
      rows: 2,
      arrows: false,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 1,
            rows: 4
          }
        }
      ]
    };

    const exhibitionSlides = exhibitions.map((exhibition) => {
      if (exhibition._id === '5eeb9d4a0b1ead4398462058') {
        return (
          <div
            key={`exhibition-${exhibition._id}`}
            className={css.slide}
            onClick={() => this.confirmExplicit(exhibition)}
            role="button"
            tabIndex="-1"
          >
            <img src={exhibition.listingThumb} alt=""/>
            <div className={cn(css.details, { [css._dark]: exhibition.darkListing })}>
              <div className={css.artistName}>{exhibition.artist.name}</div>
              <div className={css.subtitle}>{nl2br(exhibition.subtitle)}</div>
            </div>
          </div>
        );
      }

      return (
        <div key={`exhibition-${exhibition._id}`} className={css.slide}>
          <Link to={generatePath(sitemap.exhibition, { slug: exhibition._id })}>
            <img src={exhibition.listingThumb} alt=""/>
            <div className={cn(css.details, { [css._dark]: exhibition.darkListing })}>
              <div className={css.artistName}>{exhibition.artist.name}</div>
              <div className={css.subtitle}>{nl2br(exhibition.subtitle)}</div>
            </div>
          </Link>
        </div>
      );
    });

    return (
      <div id="exhibitions" className={css.wrapper}>
        <div className={css.inner}>
          {
            loading
              ? (
                <div className={css.loader}>
                  <Loading color="red"/>
                </div>
              )
              : (
                <>
                  <div className={css.header}>
                    <div className={css.aligner}/>
                    <Breakpoint m>
                      <img src={titleImage} alt=""/>
                    </Breakpoint>
                    <Breakpoint s>
                      <img src={titleImageSmall} alt=""/>
                    </Breakpoint>
                    <div className={css.total}><span>{exhibitions.length}</span> Artists</div>
                  </div>
                  <div className={css.sliderWrap}>
                    <Breakpoint m>
                      <Slider
                        {...settings}
                        ref={this.slickRef}
                        afterChange={(si) => {
                          this.setState({ slideIndex: si });
                        }}
                      >
                        {exhibitionSlides}
                      </Slider>
                    </Breakpoint>
                    <Breakpoint s>
                      {exhibitionSlides}
                    </Breakpoint>
                    <Breakpoint m>
                      <div className={css.sliderBottom}>
                        <div className={css.arrows}>
                          <div
                            className={css.left}
                            role="button"
                            tabIndex="-1"
                            onClick={() => { this.slickRef.current.slickPrev(); }}
                          />
                          <div
                            className={css.right}
                            role="button"
                            tabIndex="-1"
                            onClick={() => { this.slickRef.current.slickNext(); }}
                          />
                        </div>
                        <div className={css.counter}>
                          <span>{Math.ceil((slideIndex + 1) / 2)}</span>/{Math.ceil(exhibitions.length / 4)}
                        </div>
                      </div>
                    </Breakpoint>
                  </div>
                </>
              )
          }
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(withRouter(Exhibitions));
