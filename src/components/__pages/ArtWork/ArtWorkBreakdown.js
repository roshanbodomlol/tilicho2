import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

const ArtDetailBreakdown = ({ art }) => {
  const categoryList = art.categories.map((category) => {
    return (
      <li key={`category-item-${category}`}><NavLink to="#" className="active">{category.name}</NavLink></li>
    );
  });
  return (
    <>
      <div className="artDetailBreakdown">
        <div className="breakdownRow">
          <div className="title">
            <h6>Categories</h6>
          </div>
          <div className="content categories">
            <ul>
              {categoryList}
            </ul>
          </div>
        </div>

        {
          art.attributes.height || art.attributes.width || art.attributes.breadth
            ? (
              <div className="breakdownRow">
                <div className="title">
                  <h6>Dimension</h6>
                </div>
                <div className="content dimensions">
                  <ul>
                    {
                      art.attributes.height && <li>H: {art.attributes.height} in</li>
                    }
                    {
                      art.attributes.width && <li>W:{art.attributes.width} in</li>
                    }
                    {
                      art.attributes.breadth && <li>B: {art.attributes.breadth} in</li>
                    }
                  </ul>
                </div>
              </div>
            )
            : ''
        }
      </div>
    </>
  );
};


ArtDetailBreakdown.propTypes = {
  art: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ArtDetailBreakdown;
