import React, { Component } from 'react';
import { generatePath } from 'react-router';
import PropTypes from 'prop-types';
import withSizes from 'react-sizes';
import Breakpoint from 'react-socks';

import ArtShowcase, { ArtShowcaseItem } from '../../ArtShowcase';
import { getThumbnail } from '../../../utils/mediaUtils';
import { nl2br } from '../../../utils/stringUtils';
import Loading from '../../Loading';
import { get, post } from '../../../services/generalApiServices';
import ArtBanner from './ArtBanner';
import { CONFIG } from '../../../constants';
import ArtWorkBreakdwon from './ArtWorkBreakdown';
import ArtistShowcase from '../../ArtistShowcase';
import './ArtWork.scss';

const mapSizesToProps = (sizes => (
  {
    winWidth: sizes.width
  }
));

class ArtWork extends Component {
  constructor() {
    super();
    this.state = {
      art: {},
      similarArts: [],
      artist: null,
      isLoading: true
    };
  }

  componentDidMount() {
    this.loadArt();
    this.getSimilarArts();
  }

  componentDidUpdate(prevProps) {
    const { match } = this.props;
    if (prevProps.match.params.slug !== match.params.slug) {
      this.setState({ // eslint-disable-line
        art: {},
        similarArts: [],
        artist: null,
        isLoading: true
      }, () => {
        this.loadArt();
        this.getSimilarArts();
      });
    }
  }

  getSimilarArts() {
    const { match } = this.props;
    post(
      generatePath(CONFIG.GET_ARTWORK_BY_SIMILAR, { slug: match.params.slug }),
      (similarArts) => {
        this.setState({
          similarArts
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  loadArt() {
    const { match } = this.props;
    post(
      generatePath(CONFIG.GET_ARTWORK_BY_SLUG, { slug: match.params.slug }),
      (art) => {
        get(
          generatePath(CONFIG.GET_ARTIST_BY_SLUG, { slug: art.author.slug }),
          (artist) => {
            this.setState({
              artist,
              art,
              isLoading: false
            });
          }
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }

  render() {
    const { art, similarArts, isLoading, artist } = this.state;
    const { winWidth } = this.props;

    const productsList = similarArts.map((product) => {
      const thumbnail = getThumbnail(product.image, 'gallery');
      return (
        <ArtShowcaseItem
          key={`artist-work-${product._id}`}
          artist={product.author.name}
          artName={product.name}
          thumbnail={thumbnail}
          price={product.priceField}
          slug={product.slug}
        />
      );
    });

    let columnCount = 3;
    let gutterWidth = 14;

    if (winWidth < 991) {
      columnCount = 2;
      gutterWidth = 10;
    }
    return (
      <div id="artWorkPage">
        {
        isLoading
          ? <Loading color="red"/>
          : (
            <>
              <div className="mobilePriceContainer">
                <Breakpoint s>
                  <section className="mobilePriceSection">
                    <div className="priceContainer">
                      <h2>{art.name}</h2>
                      <h2>
                        {
                          art.priceField === 'nfs'
                            ? (
                              'Not for sale'
                            )
                            : (
                              art.priceField
                            )
                        }
                      </h2>
                    </div>
                  </section>
                </Breakpoint>
              </div>

              <section className="artGallerySection">
                <ArtBanner art={art}/>
              </section>

              <section className="detialSection">
                <div className="customContainer mediumWidthLayout">
                  <div className="sectionContent">
                    <Breakpoint m>
                      <article className="artNameArticle">
                        <h2>{art.name}</h2>
                        <h2>
                          {
                            art.priceField === 'nfs'
                              ? (
                                'Not for sale'
                              )
                              : (
                                art.priceField
                              )
                          }
                        </h2>
                      </article>
                    </Breakpoint>
                    <article className="arDetailArticle">
                      <div className="artDetailContainer">
                        <div className="artDetailItem">
                          {nl2br(art.description)}
                        </div>
                        <div className="artDetailItem">
                          <ArtWorkBreakdwon art={art}/>
                        </div>
                      </div>
                    </article>

                    <article className="artistInfoArticle">
                      <ArtistShowcase fullWidthGallery artist={artist}/>
                    </article>

                    <article className="similarArtArticle">
                      <div className="sectionTitle addTypeCenter coAddMargin coMarginSmall">
                        <h4>Similar Works</h4>
                      </div>
                      {
                        isLoading
                          ? <Loading color="red"/>
                          : (
                            <ArtShowcase
                              columnCount={columnCount}
                              gutter={gutterWidth}
                            >
                              {productsList}
                            </ArtShowcase>
                          )
                      }
                    </article>

                  </div>
                </div>
              </section>
            </>
          )
      }

      </div>
    );
  }
}

ArtWork.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  winWidth: PropTypes.number.isRequired
};

export default withSizes(mapSizesToProps)(ArtWork);
