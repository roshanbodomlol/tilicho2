import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { generatePath } from 'react-router';
import { Link } from 'react-router-dom';

import { get } from '../../../services/generalApiServices';
import { CONFIG } from '../../../constants';
import Loading from '../../Loading';
import siteMap from '../../../routing/siteMap';
import { getThumbnail } from '../../../utils/mediaUtils';
import './Search.scss';

class Search extends Component {
  state = {
    artists: [],
    products: [],
    isLoading: true
  };

  componentDidMount() {
    const { match } = this.props;
    get(
      generatePath(CONFIG.SEARCH, { term: match.params.term }),
      data => this.setState({ products: data.products, artists: data.artists, isLoading: false }),
      () => this.setState({ isLoading: false })
    );
  }

  render() {
    const { isLoading, artists, products } = this.state;
    const { match } = this.props;

    const artistList = artists.map((artist) => {
      return (
        <div className="search-result artist" key={`search-page-result-${artist._id}`}>
          <Link to={generatePath(siteMap.artist, { slug: artist.slug })}>
            <img src={`${CONFIG.BUCKET_URL}/users/${artist.picture}`} alt=""/>
            <div className="info">
              <div className="name">{artist.name}</div>
            </div>
          </Link>
        </div>
      );
    });

    const productList = products.map((product) => {
      const thumbnail = getThumbnail(product.image, 'thumbnail');
      return (
        <div className="search-result product" key={`search-page-result-${product._id}`}>
          <Link to={generatePath(siteMap.artWork, { slug: product.slug })}>
            <img src={thumbnail.url} alt=""/>
            <div className="info">
              <div className="name">{product.name}</div>
              <div className="author">{product.author.name}</div>
            </div>
          </Link>
        </div>
      );
    });
    return (
      <div id="search-page">
        {
          isLoading
            ? <Loading color="red"/>
            : (
              <div className="search-results">
                <div className="customContainer mediumWidthLayout">
                  <div className="_title">
                    Showing results for: <b>{match.params.term}</b>
                  </div>
                  {
                    products.length === 0 && artists.length === 0
                      ? (
                        <div className="no-results">
                          No results found
                        </div>
                      )
                      : (
                        <>
                          {artistList}
                          {productList}
                        </>
                      )
                  }
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

Search.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Search;
