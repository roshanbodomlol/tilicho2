import React from 'react';
import PropTypes from 'prop-types';
import BottomScrollListener from 'react-bottom-scroll-listener';

import ArtistShowcase from '../../ArtistShowcase';
import Loading from '../../Loading';

const ArtistCollection = ({ artists, isLoadingMore, loadMore, loadNoMore, viewClass }) => {
  const artistShowcaseItemList = artists.map((artist) => {
    return (
      <ArtistShowcase
        key={`artist-list-${artist._id}`}
        artist={artist}
      />
    );
  });

  return (
    <section className={viewClass}>
      <div className="customContainer">
        <div className="sectionContent">
          {artistShowcaseItemList}
          {
            loadNoMore
              ? <span style={{ textAlign: 'center', fontSize: 12 }}>No more Studios to show</span>
              : (
                <>
                  {
                    isLoadingMore
                      ? <Loading color="red" loading={isLoadingMore}/>
                      : <BottomScrollListener onBottom={loadMore} offset={300}/>
                  }
                </>
              )
          }
        </div>
      </div>
    </section>
  );
};

ArtistCollection.propTypes = {
  artists: PropTypes.arrayOf(PropTypes.object).isRequired,
  loadMore: PropTypes.func.isRequired,
  isLoadingMore: PropTypes.bool.isRequired,
  loadNoMore: PropTypes.bool.isRequired,
  viewClass: PropTypes.string
};

export default ArtistCollection;
