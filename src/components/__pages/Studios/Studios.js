import React, { Component } from 'react';
import ListViewIcon from '@material-ui/icons/ViewAgenda';
import GridViewIcon from '@material-ui/icons/Dashboard';
import classnames from 'classnames';

import { getWithParams, getWithParamsRaw } from '../../../services/generalApiServices';
import { CONFIG } from '../../../constants';
import ArtistCollection from './ArtistCollection';
import Loading from '../../Loading';
import PageHead from '../../PageHead';
import './studios.scss';

class Studios extends Component {
  state = {
    artists: [],
    isLoading: true,
    sortBy: 'follows',
    loadingMore: false,
    page: 0,
    isGridView: false
  };

  total = 999999;

  componentDidMount() {
    const { sortBy } = this.state;
    this.getInitialArtists(sortBy);
  }

  getInitialArtists = (term) => {
    const { page } = this.state;
    this.setState({
      artists: [],
      isLoading: true
    });
    getWithParamsRaw(
      CONFIG.GET_ARTISTS_SORTED,
      (res) => {
        this.total = res.total;
        this.setState({
          artists: res.message,
          isLoading: false
        });
      },
      (error) => {
        console.error(error);
      },
      {
        sortby: term,
        limit: CONFIG.ARTISTS_PAGE_LIMIT,
        skip: page * CONFIG.ARTISTS_PAGE_LIMIT
      }
    );
  }

  handleSort = (sortBy) => {
    this.setState({ sortBy });
    this.getInitialArtists(sortBy);
  }

  handleShowGrid = () => {
    this.setState({
      isGridView: true
    });
  }

  handleHideGrid = () => {
    this.setState({
      isGridView: false
    });
  }

  loadMore = () => {
    const { page, sortBy, artists } = this.state;
    const newPage = page + 1;
    this.setState({
      page: newPage,
      loadingMore: true
    }, () => {
      getWithParams(
        CONFIG.GET_ARTISTS_SORTED,
        (moreArtists) => {
          const newArtists = [...artists, ...moreArtists];
          this.setState({
            artists: newArtists,
            loadingMore: false
          });
        },
        (error) => {
          console.error(error);
        },
        {
          sortby: sortBy,
          limit: CONFIG.ARTISTS_PAGE_LIMIT,
          skip: newPage * CONFIG.ARTISTS_PAGE_LIMIT
        }
      );
    });
  }

  render() {
    const { artists, isLoading, sortBy, loadingMore, isGridView } = this.state;
    const viewClass = classnames('artistSection', {
      'grid-showing': isGridView
    });
    return (
      <div id="studiosPage">
        <PageHead title="Studios"/>
        <section className="commonSubNavSection addHalfMargin">
          <div className="customContainer">
            <div className="sectionContent">
              <ul className="artist-nav">
                <li className={sortBy === 'follows' ? 'active' : ''}>
                  <span role="button" tabIndex="-1" onClick={() => { this.handleSort('follows'); }}>Most Followers</span>
                </li>
                <li className={sortBy === 'works' ? 'active' : ''}>
                  <span role="button" tabIndex="-1" onClick={() => { this.handleSort('works'); }}>Most Works</span>
                </li>
              </ul>
              <ul className="artistView">
                <li className={!isGridView && 'active'}><ListViewIcon onClick={this.handleHideGrid}/></li>
                <li className={isGridView && 'active'}><GridViewIcon onClick={this.handleShowGrid}/></li>
              </ul>
            </div>
          </div>
        </section>

        {
          isLoading
            ? <Loading color="red"/>
            : (
              <ArtistCollection artists={artists} loadMore={this.loadMore} isLoadingMore={loadingMore} viewClass={viewClass} loadNoMore={artists.length >= this.total}/>
            )
        }
      </div>
    );
  }
}


export default Studios;
