import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { generatePath } from 'react-router';
import DownIcon from '@material-ui/icons/KeyboardArrowDown';
import classnames from 'classnames';

import sitemap from '../../../routing/siteMap';
import AdvancedAvatar from '../../AdvancedAvatar/AdvancedAvatar';
import CommonButton from '../../Button';
import FollowArtist from '../../FollowArtist';
import { CONFIG } from '../../../constants';

class ArtistSideBar extends Component {
  state = {
    isBioOpen: false
  }

  handelBioShow = () => {
    this.setState(prevState => ({
      isBioOpen: !prevState.isBioOpen
    }));
  }

  render() {
    const { isBioOpen } = this.state;
    const { artist } = this.props;
    const bioClass = classnames('sideBarGroup bioSideBarGroup', {
      'bio-showing': isBioOpen
    });

    return (
      <div className="gridItem artistSideBar">
        <div className="inner">
          <div className="sideBarGroup profileSideBarGroup">
            <div className="sideGroupContent">
              <div className="sideBarItem artistContainer">
                <AdvancedAvatar name={artist.name} imgPath={`${CONFIG.BUCKET_URL}/users/${artist.picture}`} followers={artist.follows.length}/>
                <div className="action">
                  <ul>
                    <li><FollowArtist artist={artist}/></li>
                    {/* <li>
                      <NavLink to={generatePath(sitemap.message, { to: artist.slug })}>
                        <CommonButton size="medium" mode="special">Message</CommonButton>
                      </NavLink>
                    </li> */}
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div className={bioClass}>
            <div className="sideGroupContent">
              {
                artist.info.categories.length > 0
                  && (
                    <div className="sideBarItem">
                      <div className="title">
                        <h6>Styles</h6>
                      </div>
                    </div>
                  )
              }

              {
                artist.info.biography
                && (
                  <div className="sideBarItem bioContainer">
                    <div className="title">
                      <h6>Biography</h6>
                    </div>
                    <div className="content">
                      {
                        artist.info.biography
                      }
                    </div>
                  </div>
                )
              }
            </div>
          </div>

          <div className="sideGroupAction">
            <DownIcon onClick={this.handelBioShow}/>
          </div>
        </div>
      </div>
    );
  }
}

ArtistSideBar.propTypes = {
  artist: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ArtistSideBar;
