import React, { Component } from 'react';
import { Switch, Route, Redirect, NavLink } from 'react-router-dom';
import { generatePath } from 'react-router';
import PropTypes from 'prop-types';

import sitemap from '../../../routing/siteMap';
import { get } from '../../../services/generalApiServices';
import { CONFIG } from '../../../constants';
import ArtistWork from './ArtistWork';
import ArtistFeed from './ArtistFeed';
import PageHead from '../../PageHead';
import ArtistSideBar from './ArtistSideBar';
import './artist.scss';

class Artist extends Component {
  state = {
    artist: {},
    loading: true
  }

  componentDidMount() {
    const { match, history } = this.props;
    get(
      generatePath(CONFIG.GET_ARTIST_BY_SLUG, { slug: match.params.slug }),
      (response) => {
        this.setState({ artist: response, loading: false });
      },
      (error) => {
        this.setState({ loading: false });
        console.error(error);
        history.push('/404');
      }
    );
  }

  render() {
    const { match } = this.props;
    const { artist, loading } = this.state;
    // const stylesList = artist.info.categories
    return (
      <div id="artistPage">
        {
          loading
            ? ''
            : (
              <div className="commonGidContainer typeHeaderUp">
                <PageHead imgPath={artist.cover && `${CONFIG.BUCKET_URL}/users/${artist.cover}`}/>
                <ArtistSideBar artist={artist}/>
                <div className="gridItem mainContent">
                  <div className="artist-bar">
                    <div className="sectionContent">
                      <ul>
                        <li><NavLink to={generatePath(sitemap.artistWork, { slug: artist.slug })}>Work</NavLink></li>
                        <li><NavLink to={generatePath(sitemap.artistFeed, { slug: artist.slug })}>Feed</NavLink></li>
                      </ul>
                    </div>
                  </div>

                  <Switch>
                    <Route exact path={sitemap.artistWork} render={props => <ArtistWork {...props} artist={artist}/>}/>
                    <Route exact path={sitemap.artistFeed} render={props => <ArtistFeed {...props} artist={artist}/>}/>
                    <Redirect to={generatePath(sitemap.artistWork, { slug: match.params.slug })}/>
                  </Switch>
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

Artist.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Artist;
