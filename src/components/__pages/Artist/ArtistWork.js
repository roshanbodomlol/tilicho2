import React from 'react';
import PropTypes from 'prop-types';
import withSizes from 'react-sizes';

import ArtShowcase, { ArtShowcaseItem } from '../../ArtShowcase';
import { getThumbnail } from '../../../utils/mediaUtils';

const mapSizesToProps = (sizes => (
  {
    winWidth: sizes.width
  }
));

const ArtistWork = ({ artist, winWidth }) => {
  const artShowcaseItemList = artist.products.map((product) => {
    const thumbnail = getThumbnail(product.image, 'gallery');
    return (
      <ArtShowcaseItem
        readOnly={false}
        key={`artist-work-${product._id}`}
        artist={artist.name}
        artistId={artist.id}
        artName={product.name}
        thumbnail={thumbnail}
        price={product.priceField}
        slug={product.slug}
      />
    );
  });

  let columnCount = 4;
  if (winWidth < 1440) {
    columnCount = 4;
  }
  if (winWidth < 1199) {
    columnCount = 3;
  }
  if (winWidth < 991) {
    columnCount = 2;
  }
  return (
    <section className="artistWorkSection">
      <div className="customContainer">
        <div className="sectionContent">
          <ArtShowcase
            columnCount={columnCount}
            gutter={17}
          >
            {artShowcaseItemList}
          </ArtShowcase>
        </div>
      </div>
    </section>
  );
};

ArtistWork.propTypes = {
  artist: PropTypes.objectOf(PropTypes.any).isRequired,
  winWidth: PropTypes.number.isRequired
};

export default withSizes(mapSizesToProps)(ArtistWork);
