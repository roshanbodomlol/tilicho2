import React from 'react';
import { NavLink } from 'react-router-dom';

import sitemap from '../../../routing/siteMap';
import styles from './NotFound.module.scss';

const NotFound = () => (
  <NavLink to={sitemap.home}>
    <div className={styles.notfound}>
      OOPS!
    </div>
  </NavLink>
);

export default NotFound;
