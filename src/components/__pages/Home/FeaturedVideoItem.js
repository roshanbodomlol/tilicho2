import React from 'react';
import PropTypes from 'prop-types';

import './Home.scss';

const FeaturedVideoItem = ({ video }) => {
  return (
    <div className="videoItem">
      <iframe
        title="featured-video"
        width="100%"
        src={`https://www.youtube.com/embed/${video}?rel=0&amp;controls=0&amp;showinfo=0`}
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
      />
    </div>
  );
};

FeaturedVideoItem.propTypes = {
  video: PropTypes.string.isRequired
};


export default FeaturedVideoItem;
