import React from 'react';
import { connect } from 'react-redux';
import { objectOf, any } from 'prop-types';

import Banner from './Banner';
import ArtCategory from './ArtCategory';
import ArtFromFollowed from '../../ArtFromFollowed';
import StudiosToFollow from '../../StudiosToFollow';
import FeaturedArtworks from '../../FeaturedArtworks';
import ArtList from '../../ArtList';
import BhittaBanner from './BhittaBanner';
import ScrollTop from '../../ScrollToTop';

const mapStateToProps = ({ user }) => ({ user });

const Home = ({ user }) => {
  return (
    <div id="homePage">
      {/* <Banner/> */}
      <ScrollTop/>
      <BhittaBanner/>
      <ArtCategory/>
      {
        user && <ArtFromFollowed/>
      }
      {
        user && <StudiosToFollow/>
      }
      <FeaturedArtworks/>
      <ArtList
        categoryId="5a8e98c45b07a232a8e548fa"
        name="Paintings"
      />
      <ArtList
        categoryId="5a8e98cf5b07a232a8e548fb"
        name="Photography"
      />
      <ArtList
        categoryId="5a8e98d85b07a232a8e548fc"
        name="Sculpture"
      />
      <ArtList
        categoryId="5b5f3c530757da2edcb7abba"
        name="Design"
      />
    </div>
  );
};

Home.propTypes = {
  user: objectOf(any)
};

Home.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(Home);
