import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import sitemap from '../../../routing/siteMap';
import { getThumbnail } from '../../../utils/mediaUtils';
import ContentScroller from '../../ContentScroller';
import { CONFIG } from '../../../constants';
import { getCategories, getPopularProductsByCategory, getProductsFromFollowed } from '../../../services/productServices';
import './Home.scss';

class PopularWorks extends Component {
  state = {
    loading: true,
    loadingProducts: true,
    categories: [],
    products: [],
    activeCategory: 'all'
  }

  componentDidMount() {
    const { activeCategory } = this.state;
    this.setState({ loading: true });
    getCategories(
      (categories) => {
        this.setState({
          categories,
          loading: false
        });
      },
      (e) => {
        this.setState({ loading: false });
        console.error(e);
      }
    );
    this.fetchProductsByCategory(activeCategory);
    getProductsFromFollowed()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  fetchProductsByCategory = (activeCategory) => {
    this.setState({ loadingProducts: true });
    getPopularProductsByCategory(
      activeCategory,
      (products) => {
        this.setState({
          loadingProducts: false,
          products
        });
      },
      (e) => {
        this.setState({ loadingProducts: false });
        console.error(e);
      }
    );
  }

  changeCategory = (id) => {
    this.setState({ activeCategory: id });
    this.fetchProductsByCategory(id);
  }

  render() {
    const { categories, loading, activeCategory, products, loadingProducts } = this.state;
    const items = !loading && products.map((item) => {
      const thumbnail = getThumbnail(item.image, 'listing');
      return (
        <div key={`popular-item-${item._id}`} className="popularItem">
          <Link to={generatePath(sitemap.artWork, { slug: item.slug })}>
            <img src={`${CONFIG.BUCKET_URL}/${item.image.set.slug}/${thumbnail.fileName}`} alt=""/>
          </Link>
        </div>
      );
    });
    return (
      <section className="popularWorksSection">
        <div className="customContainer">
          <div className="sectionContent">
            <div className="popularWorks">
              {
                loading
                  ? 'Loading'
                  : (
                    <ContentScroller
                      title="Popular Works"
                      tabs={categories}
                      activeTab={activeCategory}
                      setActive={this.changeCategory}
                      loading={loadingProducts}
                    >
                      {items}
                    </ContentScroller>
                  )
              }
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default PopularWorks;
