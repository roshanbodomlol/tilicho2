import React, { Component } from 'react';

import { ThreeDotsRed } from '../../ThreeDots';
import FeaturedVideoItem from './FeaturedVideoItem';
import './Home.scss';


class FeaturedVideo extends Component {
  state = {
    videoItems: [],
    loading: true
  }

  componentDidMount() {
    this.setState({
      videoItems: [
        {
          video: 'wnLcCEavyiY'
        }
      ],
      loading: false
    });
  }

  render() {
    const { videoItems, loading } = this.state;
    const videoItemsMultiple = videoItems.map((item, index) => (
      <FeaturedVideoItem key={`video-item-${index}`} video={item.video}/>
    ));
    return (
      <section className="commonVideoSection">
        <div className="customContainer addPaddingTopBottom mediumWidthLayout">
          <div className="sectionTitle addTypeCenter addWhiteColor addBottomLine coAddMargin">
            <h5>Featured Video</h5>
          </div>
          <div className="sectionContent">
            <div className="videoContainer">
              {
                loading
                  ? (
                    <div>
                      <ThreeDotsRed variant="flashing"/>
                    </div>
                  )
                  : videoItemsMultiple
              }
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default FeaturedVideo;
