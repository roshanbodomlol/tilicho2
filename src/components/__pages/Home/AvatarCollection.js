import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import { CONFIG } from '../../../constants';
import { get } from '../../../services/generalApiServices';
import sitemap from '../../../routing/siteMap';
import ContentScroller from '../../ContentScroller';
import { ThreeDotsRed } from '../../ThreeDots';
import AdvancedAvatar from '../../AdvancedAvatar/AdvancedAvatar';
import FollowArtist from '../../FollowArtist';
import './Home.scss';

class AvatarCollection extends Component {
  state = {
    loading: true,
    artists: []
  }

  componentDidMount() {
    get(
      CONFIG.GET_TOP_ARTISTS,
      (response) => {
        this.setState({
          artists: response,
          loading: false
        });
      },
      (error) => {
        this.setState({
          loading: false
        });
        console.log(error);
      }
    );
  }

  render() {
    const { artists, loading } = this.state;
    const avatarItemsMultiple = artists.length > 0 && artists.map((item, index) => (
      <div className="artist" key={`nav-item-${index}`}>
        <Link to={generatePath(sitemap.artist, { slug: item.slug })}>
          <AdvancedAvatar
            imgPath={`${CONFIG.BUCKET_URL}/users/${item.picture}`}
            followers={item.follows.length}
            works={item.products.length}
            name={item.name}
            className="artistBlock"
          />
        </Link>
        <FollowArtist artist={item}/>
      </div>
    ));
    return (
      <section className="followArtistSection">
        <div className="customContainer">
          <div className="inner">
            <div className="sectionContent">
              <div className="avatarCollectionContainer">
                {
                  loading
                    ? (
                      <div>
                        <ThreeDotsRed variant="flashing"/>
                      </div>
                    )
                    : (
                      <>
                        {
                          avatarItemsMultiple
                          && (
                            <ContentScroller
                              title="Artists to Follow"
                            >
                              {avatarItemsMultiple}
                            </ContentScroller>
                          )
                        }
                      </>
                    )
                }
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default AvatarCollection;
