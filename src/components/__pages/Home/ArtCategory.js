import React from 'react';
import { NavLink } from 'react-router-dom';

import HomeSection from '../../HomeSection';
import './Home.scss';

const ArtCategory = () => {
  return (
    <div className="art-categories">
      <HomeSection fullWidth>
        <section className="categorySection">
          <div className="sectionContent">
            <ul className="categoryContainer">
              <li><NavLink to="/gallery/painting">Painting</NavLink></li>
              <li><NavLink to="/gallery/sculpture">Sculpture</NavLink></li>
              <li><NavLink to="/gallery/photography">Photography</NavLink></li>
              <li><NavLink to="/gallery/design">Design</NavLink></li>
            </ul>
          </div>
        </section>
      </HomeSection>
    </div>
  );
};

export default ArtCategory;
