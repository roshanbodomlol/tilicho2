import React from 'react';
import { Link } from 'react-router-dom';
import Breakpoint from 'react-socks';
import moment from 'moment';

import sitemap from '../../../routing/siteMap';
import Banner1 from '../../../assets/img/divya-desktop.jpg';
import Banner2 from '../../../assets/img/divya-mobile.jpg';

const urls = [
  '/exhibitions/5ede188af30c3727337562ce',
  '/exhibitions/5ede188af30c3727337562ce',
  '/exhibitions/5ee4d935a299707f4faf4a41',
  '/exhibitions/5ee1a942f30c3727337564e1',
  '/exhibitions/5ede15e3f30c3727337562c6',
  '/exhibitions/5edf5c7df30c3727337563b4',
  '/exhibitions/5ee72ff1a299707f4faf4aa7',
  '/exhibitions/5ee2fcae3648de6a2a42e19d',
  '/exhibitions/5eece4ed0b1ead43984620c3',
  '/exhibitions/5eeb965e0b1ead4398462051',
  '/exhibitions/5ede845ef30c3727337562df',
  '/exhibitions/5ee19afef30c3727337564b4',
  '/exhibitions/5eeba4550b1ead4398462072',
  '/exhibitions/5ee1a514f30c3727337564cf',
  '/exhibitions/5ee234683648de6a2a42e190',
  '/exhibitions/5ee1a082f30c3727337564c4',
  '/exhibitions/5eeb9d4a0b1ead4398462058',
  '/exhibitions/5ee39a615816e271dd79be7c',
  '/exhibitions/5ee393d45816e271dd79be76',
  '/exhibitions/5ee4ed86a299707f4faf4a71',
  '/exhibitions/5ee10fd0f30c3727337564a5',
  '/exhibitions/5ee366051c16e470f680b37ax'
];

const startDate = moment('07-02-2020', 'MM-DD-YYYY');
const dayNumber = moment().diff(startDate, 'd');

const BhittaBanner = () => (
  <div id="bhitta-banner">
    <div className="customContainer">
      <Link to="/exhibitions/5f6d8eee5585452ea361b614">
        <Breakpoint s>
          <img src={Banner2} alt=""/>
        </Breakpoint>
        <Breakpoint m>
          <img src={Banner1} alt=""/>
        </Breakpoint>
      </Link>
    </div>
  </div>
);

export default BhittaBanner;
