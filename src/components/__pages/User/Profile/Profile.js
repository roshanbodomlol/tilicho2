import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { generatePath } from 'react-router';
import PropTypes from 'prop-types';

import sitemap from '../../../../routing/siteMap';
import { isUserArtist } from '../../../../utils/userUtils';
import AdvancedAvatar from '../../../AdvancedAvatar/AdvancedAvatar';
import Loading from '../../../Loading';
import CommonButton from '../../../Button';
import { CONFIG } from '../../../../constants';
import NewFeed from '../../Feed/NewFeed';
import PageHead from '../../../PageHead';
import FeedCollection from '../../Feed/FeedCollection';
import { getWithParams } from '../../../../services/generalApiServices';
import './Profile.scss';

const mapStateToProps = state => ({
  user: state.user
});

class Profile extends Component {
  state = {
    posts: [],
    isLoading: true,
    loadingMore: false
  };

  total = CONFIG.FEED_LIMIT;

  componentDidMount() {
    this.getPosts();
  }

  getPosts = () => {
    const { user } = this.props;
    getWithParams(
      generatePath(CONFIG.GET_POSTS_BY_ARTIST, { id: user.id, total: this.total }),
      (response) => {
        this.setState({ posts: response, isLoading: false, loadingMore: false });
      },
      (err) => {
        console.log(err);
        this.setState({ isLoading: false });
      }
    );
  }

  getMorePosts = () => {
    this.total = this.total + CONFIG.FEED_LIMIT;
    this.setState({ loadingMore: true }, () => {
      this.getPosts();
    });
  }

  render() {
    const { posts, isLoading, loadingMore } = this.state;
    const { user } = this.props;
    return (
      <div id="profilePage">
        <div className="commonGidContainer typeHeaderUp">
          <PageHead imgPath={`${CONFIG.BUCKET_URL}/users/${user.picture}`}/>
          <div className="gridItem artistSideBar">
            <div className="inner">
              <div className="sideBarGroup profileSideBarGroup">
                <div className="sideGroupContent">
                  <div className="sideBarItem artistContainer">
                    <AdvancedAvatar name={user.name} imgPath={`${CONFIG.BUCKET_URL}/users/${user.picture}`}/>
                    <div className="action">
                      <ul>
                        <li>
                          <NavLink to={sitemap.editInformation} activeClassName="active">
                            <CommonButton size="medium" mode="specialBlack">Edit Profile</CommonButton>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="gridItem mainContent">
            <section className="feedSection">
              <div className="customContainer smallWidthLayout">
                <div className="sectionContent">
                  {
                    !isUserArtist(user.role) && (
                      <Link to={sitemap.getStudio}>
                        <div className="getFeedTitle">
                          <h3>Get a studio</h3>
                        </div>
                      </Link>
                    )
                  }
                  {
                    user && <NewFeed refresh={this.getPosts}/>
                  }
                  {
                    isLoading
                      ? <Loading color="red"/>
                      : <FeedCollection posts={posts} refresh={this.getPosts} loadingMore={loadingMore} loadMore={this.getMorePosts}/>
                  }
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(Profile);
