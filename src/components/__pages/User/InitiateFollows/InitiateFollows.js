import React, { Component } from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import { CONFIG } from '../../../../constants';
import { get, postAuth } from '../../../../services/generalApiServices';
import sitemap from '../../../../routing/siteMap';
import Loading from '../../../Loading';
import AdvancedAvatar from '../../../AdvancedAvatar/AdvancedAvatar';
import Button from '../../../Button';
import store from '../../../../redux';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import './InitiateFollows.scss';


class InitiateFollows extends Component {
  state = {
    isLoading: true,
    artists: [],
    categories: [],
    selectedArtists: [],
    updating: false
  }

  componentDidMount() {
    // make body unscrollable
    document.body.classList.add('no-scroll');
    get(
      CONFIG.GET_ALL_ARTISTS,
      (response) => {
        const filteredArtists = response.filter((artist) => {
          if (artist.settings) {
            if (artist.settings.private) {
              return false;
            }

            return true;
          }

          return true;
        });
        this.setState({
          artists: filteredArtists,
          isLoading: false
        });
      },
      (error) => {
        this.setState({
          isLoading: false
        });
        console.log(error);
      }
    );
  }

  componentWillUnmount() {
    // make body unscrollable
    document.body.classList.remove('no-scroll');
  }

  getCategories = () => {
    get(
      CONFIG.GET_CATEGORIES,
      (categories) => {
        this.setState({
          categories
        });
      },
      (e) => {
        console.log(e);
      }
    );
  }

  handleArtistClick = (artistId) => {
    const { selectedArtists } = this.state;
    const newSelection = [...selectedArtists];
    const artistIndex = newSelection.findIndex(id => id === artistId);
    if (artistIndex < 0) {
      newSelection.push(artistId);
    } else {
      newSelection.splice(artistIndex, 1);
    }
    this.setState({
      selectedArtists: newSelection
    });
  }

  handleConfirm = () => {
    const { selectedArtists } = this.state;
    if (selectedArtists.length < 1) {
      store.dispatch(showGlobalSnack('error', 'Please select at least one artist to follow', 3000));
    } else {
      const data = new FormData();
      for (let i = 0; i < selectedArtists.length; i++) {
        data.append('follows[]', selectedArtists[i]);
      }
      this.setState({ updating: true });
      postAuth(
        CONFIG.INITIATE_FOLLOWS,
        () => {
          window.location.reload();
        },
        (e) => {
          console.error(e);
          store.dispatch('error', 'Something went wrong. Please try again', 3000);
        },
        data
      );
    }
  }

  handleSkip = () => {
    this.setState({ updating: true });
    postAuth(
      CONFIG.INITIATE_FOLLOWS,
      () => {
        window.location.reload();
      },
      (e) => {
        console.error(e);
        store.dispatch('error', 'Something went wrong. Please try again', 3000);
      }
    );
  }

  render() {
    const { isLoading, artists, categories, selectedArtists, updating } = this.state;
    const artistsList = artists.length > 0 && artists.map((item, index) => {
      const artistItemClasses = classnames('artistItem', {
        selected: selectedArtists.includes(item._id)
      });
      return (
        <div
          key={`nav-item-${index}`}
          className={artistItemClasses}
          role="button"
          tabIndex="-1"
          onClick={() => this.handleArtistClick(item._id)}
        >
          <div className="artistInner">
            <AdvancedAvatar
              imgPath={`${CONFIG.BUCKET_URL}/users/${item.picture}`}
              followers={item.follows.length}
              works={item.products.length}
              name={item.name}
            />
          </div>
        </div>
      );
    });
    const categoryList = categories.map(category => (
      <li key={`gallery-cat-${category._id}`}>
        <Link exact to={generatePath(sitemap.InitiateFollows, { cat: category.slug })}><span>{category.name}</span></Link>
      </li>
    ));
    return (
      <div id="InitiateFollowsPage">
        <section className="commonSubNavSection addHalfMargin typeCategories">
          <div className="customContainer">
            <div className="sectionContent">
              <div className="subNavItem">
                <ul>
                  <li><Link exact to="/gallery/all"><span>All</span></Link></li>
                  {categoryList}
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section className="artistCollectionSection">
          <div className="customContainer">
            <div className="sectionContent">
              <div className="artistCollectionContainer">
                {
                  isLoading
                    ? (
                      <Loading color="red"/>
                    )
                    : (
                      artistsList
                    )
                }
              </div>
            </div>
          </div>
        </section>
        <div className="confirmation-bar">
          <span>Select artists you want to follow</span>
          <Button loading={updating} onClick={this.handleConfirm}>Confirm</Button>
          <span role="button" tabIndex="-1" onClick={this.handleSkip}>Skip</span>
        </div>
      </div>
    );
  }
}

export default InitiateFollows;
