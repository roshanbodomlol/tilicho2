handleArtistClick = (id) => {
  const {selectedArtists} = this.state;
  const newSelection = [...selectedArtists];
  if (newSelection.findIndex(selectedId => selectedId === id) < 0) {
    newSelection.push(id);
  } else {
    newSelection.splice(newSelection.findIndex(selectedId => selectedId === id), 1);
  }
  this.setState({selectedArtists: newSelection});
}
