import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { generatePath } from 'react-router';
import { withRouter } from 'react-router-dom';

import { get, postAuthMultipart } from '../../../../services/generalApiServices';
import { CONFIG } from '../../../../constants';
import Header from '../../../UserPageHeader';
import ContentCard from '../../../ContentCard';
import { Input } from '../../../Form';
import FancyNav from '../../../FancyNav';
import ImageUploader from '../../../ImageUploader';
import sitemap from '../../../../routing/siteMap';
import store from '../../../../redux';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import './Upload.scss';

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCategories: [],
      images: [],
      coverImageIndex: 0,
      name: null,
      description: null,
      price: null,
      stock: null,
      width: '',
      height: '',
      breadth: '',
      weight: '',
      mainCategory: null,
      subCategory: null,
      uploading: false
    };
  }

  componentDidMount() {
    get(
      CONFIG.GET_CATEGORIES_SORTED,
      (allCategories) => {
        this.setState({
          allCategories
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }

  handleCategorySelect = (categoryId) => {
    this.setState({ mainCategory: categoryId });
  }

  handleSubCategoryClick = (categoryId) => {
    const { subCategory } = this.state;
    this.setState({
      subCategory: categoryId === subCategory ? null : categoryId
    });
  }

  handleAddImage = (file, dataUrl, id) => {
    const { images } = this.state;
    const newImagesState = [...images];
    newImagesState.push({
      id,
      file,
      dataUrl
    });
    this.setState({ images: newImagesState });
  }

  handleSetCoverImage = (coverImageIndex) => {
    this.setState({ coverImageIndex });
  }

  handleRemoveImage = (imageId) => {
    const { images, coverImageIndex } = this.state;
    const newImages = [...images];
    const imageIdIndex = newImages.findIndex(image => image.id === imageId);
    const coverImageId = newImages[coverImageIndex].id;
    newImages.splice(imageIdIndex, 1);
    const newCoverImageIndex = newImages.findIndex(image => image.id === coverImageId);
    this.setState({
      images: newImages,
      coverImageIndex: newCoverImageIndex < 0 ? 0 : newCoverImageIndex
    });
  }

  handleUpload = () => {
    const {
      images,
      coverImageIndex,
      name,
      description,
      mainCategory,
      subCategory,
      price,
      stock,
      width,
      height,
      breadth,
      weight
    } = this.state;
    const { history } = this.props;

    const data = new FormData();

    // validate images
    // upload images limit
    if (images.length > CONFIG.UPLOAD_ART_IMAGES_LIMIT) {
      store.dispatch(showGlobalSnack('error', `You can only upload ${CONFIG.UPLOAD_ART_IMAGES_LIMIT} images`, 3000));
      return;
    }

    if (images.length < 1) {
      store.dispatch(showGlobalSnack('error', 'You have to upload at least one image', 3000));
      return;
    }

    // image
    data.append('image', images[coverImageIndex].file);

    // images
    for (let i = 0; i < images.length; i++) {
      if (i !== coverImageIndex) {
        data.append('images', images[i].file);
      }
    }

    // name
    if (name) {
      data.append('name', name);
    } else {
      store.dispatch(showGlobalSnack('error', 'Name is requred', 3000));
      return;
    }

    // description
    if (description) {
      data.append('description', description);
    } else {
      // store.dispatch(showGlobalSnack('error', 'Description is required', 3000));
      // return;
    }

    // categories
    if (mainCategory) {
      data.append('categories[]', mainCategory);
    } else {
      store.dispatch(showGlobalSnack('error', 'Please select a category.', 3000));
      return;
    }

    if (subCategory) {
      data.append('categories[]', subCategory);
    } else {
      store.dispatch(showGlobalSnack('error', 'Please select a sub category.', 3000));
      return;
    }

    // price
    if (price || price === 0) {
      data.append('price', price);
    } else {
      store.dispatch(showGlobalSnack('error', 'Please provide the price. Enter 0 to list as Not For Sale', 3000));
      return;
    }

    // stock
    if (stock || stock === 0) {
      data.append('stock', stock);
    } else {
      store.dispatch(showGlobalSnack('error', 'Please provide the stock amount', 3000));
      return;
    }

    data.append('attributes[width]', width);
    data.append('attributes[height]', height);
    data.append('attributes[breadth]', breadth);
    data.append('attributes[weight]', weight);

    // if everything ok
    this.setState({
      uploading: true
    }, () => {
      postAuthMultipart(
        CONFIG.UPLOAD_ART,
        (res) => {
          store.dispatch(showGlobalSnack('normal', 'You have successfully uploaded your artwork', 3000));
          history.push(generatePath(sitemap.artWork, { slug: res.data.productSlug }));
        },
        (e) => {
          console.error(e);
          store.dispatch(showGlobalSnack('error', 'Something went wrong. Please try again.', 3000));
        },
        data
      );
    });
  }

  render() {
    const { history, user } = this.props;
    const {
      allCategories,
      images,
      coverImageIndex,
      name,
      description,
      mainCategory,
      subCategory,
      price,
      stock,
      width,
      height,
      breadth,
      weight,
      uploading
    } = this.state;
    const topCategories = allCategories.filter(category => !category.parent);
    const categoryNavList = topCategories.map(category => (
      {
        name: category.name,
        key: category._id
      }
    ));
    const categoryChildren = allCategories.filter(category => category.parent && category.parent._id === mainCategory);
    const categoryChildrenList = categoryChildren.map((category) => {
      const buttonClasses = classnames('category-select', {
        active: category._id === subCategory
      });
      return (
        <button
          key={`category-select-${category._id}`}
          className={buttonClasses}
          onClick={() => { this.handleSubCategoryClick(category._id); }}
        >
          {category.name}
        </button>
      );
    });

    return (
      <div id="page-upload-art">
        <Header
          title="Upload Art"
          subTitle="Upload high quality and original content"
          cancel={() => history.push(generatePath(sitemap.artist, { slug: user.slug }))}
          publish={this.handleUpload}
          loading={uploading}
        />

        <section className="commonFormSection">
          <div className="customContainer smallWidthLayout">
            <ContentCard title="Upload Images">
              <ImageUploader
                images={images}
                coverImageIndex={coverImageIndex}
                addImage={this.handleAddImage}
                setCoverImage={this.handleSetCoverImage}
                removeImage={this.handleRemoveImage}
              />
            </ContentCard>
            <ContentCard title="Basic Information">
              <Input
                required
                label="Name"
                type="text"
                name="name"
                value={name}
                onChange={(e) => { this.setState({ name: e.target.value }); }}
              />
              <Input
                label="Description"
                type="textarea"
                name="description"
                value={description}
                onChange={(e) => { this.setState({ description: e.target.value }); }}
              />
            </ContentCard>
            <ContentCard title="Categories">
              {
                categoryNavList.length > 0
                && (
                  <FancyNav
                    activeItemKey={mainCategory}
                    items={categoryNavList}
                    onClick={this.handleCategorySelect}
                  />
                )
              }
              <div className="category-list">
                {categoryChildrenList}
              </div>
            </ContentCard>

            <ContentCard title="Price and Stock" className="typeThreeCol" subtitle="Price shown will be inclusive of all charges">
              <div className="typeThreeCol">
                <Input
                  required
                  label="Price (NPR)"
                  type="number"
                  name="price"
                  value={price}
                  subtitle="Enter 0 to list as Not For Sale"
                  min={0}
                  onChange={(e) => { this.setState({ price: e.target.value }); }}
                />
                <Input
                  required
                  label="Stock"
                  type="number"
                  name="stock"
                  value={stock}
                  min={0}
                  onChange={(e) => { this.setState({ stock: e.target.value }); }}
                />
              </div>
            </ContentCard>

            <ContentCard title="Dimensions" subtitle="All dimensions are in inches">
              <div className="typeThreeCol">
                <Input
                  label="Height"
                  type="number"
                  name="height"
                  value={height}
                  onChange={(e) => { this.setState({ height: e.target.value }); }}
                />
                <Input
                  label="Width"
                  type="number"
                  name="width"
                  value={width}
                  onChange={(e) => { this.setState({ width: e.target.value }); }}
                />
                <Input
                  label="Breadth"
                  type="number"
                  name="breadth"
                  value={breadth}
                  onChange={(e) => { this.setState({ breadth: e.target.value }); }}
                />
                <Input
                  label="Weight"
                  type="number"
                  name="weight"
                  value={weight}
                  onChange={(e) => { this.setState({ weight: e.target.value }); }}
                />
              </div>
            </ContentCard>
          </div>
        </section>
      </div>
    );
  }
}

Upload.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default withRouter(Upload);
