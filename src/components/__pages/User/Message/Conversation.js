import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { generatePath } from 'react-router';
import _ from 'lodash';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowLeftIcon from '@material-ui/icons/ArrowBack';

import SocketContext from '../../../../socket';
import Loading from '../../../Loading';
import CommonButton from '../../../Button';
import Avatar from '../../../Avatar';
import store from '../../../../redux';
import sitemap from '../../../../routing/siteMap';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import { get, getWithAuth, postAuth } from '../../../../services/generalApiServices';
import { CONFIG } from '../../../../constants';

const mapStateToProps = ({ user }) => ({
  user
});

class Conversation extends Component {
  state = {
    activeConversation: [],
    to: null,
    loadingTo: true,
    sending: false,
    newMessage: ''
  };

  componentDidMount() {
    const { match, showConverstation } = this.props;
    if (match.params.to) {
      this.getParticipantUser();
      this.getConversation();
      this.connect();
    }
  }

  componentDidUpdate(preVprops) {
    const { match } = this.props;
    if (preVprops.match.params.to !== match.params.to) {
      this.getParticipantUser();
      this.getConversation();
    }
  }

  connect = () => {
    const { socket } = this.props;
    if (socket) {
      socket.on('newMessage', () => {
        this.refreshConversation();
      });
    }
  }

  getParticipantUser = () => {
    const { match, history, showConversation } = this.props;
    this.setState({
      activeConversation: [],
      to: null,
      loadingTo: true
    });
    get(
      generatePath(CONFIG.GET_USER_BY_SLUG, { slug: match.params.to }),
      (to) => {
        this.setState({ to, loadingTo: false });
        showConversation();
      },
      (e) => {
        console.error(e);
        store.dispatch(showGlobalSnack('error', 'Something went wrong', 3000));
        history.push(sitemap.messages);
      }
    );
  }

  getConversation = () => {
    const { match, user } = this.props;
    this.setState({
      activeConversation: [],
      to: null,
      loadingTo: true
    });
    if (user) {
      getWithAuth(
        generatePath(CONFIG.GET_MESSAGES, { to: match.params.to }),
        ((activeConversation) => {
          this.setState({ activeConversation });
          if (activeConversation && !activeConversation.seen.includes(user.id)) {
            this.makeConversationSeen(activeConversation._id);
          }
        }),
        (e) => {
          console.error(e);
          store.dispatch(showGlobalSnack('error', 'Something went wrong. Please reload and try again.', 3000));
        }
      );
    }
  }

  refreshConversation = () => {
    const { match, user } = this.props;
    if (user) {
      getWithAuth(
        generatePath(CONFIG.GET_MESSAGES, { to: match.params.to }),
        ((activeConversation) => {
          this.setState({ activeConversation });
          if (activeConversation.seen.includes(user.id)) {
            this.makeConversationSeen(activeConversation._id);
          }
        }),
        (e) => {
          console.error(e);
          store.dispatch(showGlobalSnack('error', 'Something went wrong. Please reload and try again.', 3000));
        }
      );
    }
  }

  makeConversationSeen = (id) => {
    postAuth(
      generatePath(CONFIG.MAKE_CONVERSATION_SEEN, { id }),
      () => {},
      (e) => {
        console.error(e);
      }
    );
  }

  createMessage = (e) => {
    e.preventDefault();
    this.setState({ sending: true });
    const { to, newMessage } = this.state;
    postAuth(
      CONFIG.CREATE_MESSAGE,
      () => {
        this.setState({ sending: false, newMessage: '' });
        this.refreshConversation();
      },
      (err) => {
        console.log(err);
        store.dispatch(showGlobalSnack('error', 'Something went wrong.', 3000));
        this.setState({ sending: true });
      },
      {
        to: to._id,
        message: newMessage
      }
    );
  }

  render() {
    const { activeConversation, to, loadingTo, sending, newMessage } = this.state;
    const { user, close } = this.props;
    let messages = [];
    if (!_.isEmpty(activeConversation)) {
      messages = activeConversation.messages.map((message, index) => {
        const messageClass = message.from === user.id ? 'dir_right' : '';
        return (
          <div className={`messageItem ${messageClass}`} key={`message-${index}`}>
            <div className="inner">
              {
                messageClass !== 'dir_right'
                  ? (
                    <>
                      <Avatar imgPath={`${CONFIG.BUCKET_URL}/users/${to.picture}`}/>
                      <div className="chatBox">
                        <div className="chatItem">
                          <div className="itemInner">
                            <p>{message.message}</p>
                          </div>
                        </div>
                      </div>
                    </>
                  )
                  : (
                    <div className="chatBox">
                      <div className="chatItem">
                        <div className="itemInner">
                          <p>{message.message}</p>
                        </div>
                      </div>
                    </div>
                  )
              }
            </div>
          </div>
        );
      });
    }

    return (
      <>
        {
          loadingTo
            ? <Loading color="red"/>
            : (
              <>
                <div className="title">
                  <div className="backIcon">
                    <ArrowLeftIcon onClick={close}/>
                  </div>
                  <h5>{to.name}</h5>
                  <div className="deleteIcon" style={{ opacity: 0 }}>
                    <DeleteIcon/>
                  </div>
                </div>
                <div className="content">
                  <div className="messageThread">
                    <div className="messages">
                      {messages}
                    </div>
                    <div className="newmessages">
                      <form onSubmit={this.createMessage}>
                        <input disabled={sending} type="text" value={newMessage} onChange={(e) => { this.setState({ newMessage: e.target.value }); }}/>
                        <CommonButton size="chat" mode="primary" loading={sending} type="submit">Send</CommonButton>
                      </form>
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </>
    );
  }
}

const SocketedConversation = props => (
  <SocketContext.Consumer>
    {
      socket => <Conversation {...props} socket={socket}/>
    }
  </SocketContext.Consumer>
);

Conversation.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  showConversation: PropTypes.bool.isRequired,
  socket: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(SocketedConversation);
