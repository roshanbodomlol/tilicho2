import React from 'react';

const Blank = () => (
  <div id="messages-blank-state">
    <p>Select a recipient</p>
  </div>
);

export default Blank;
