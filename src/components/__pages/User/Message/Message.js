import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Link, Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import classnames from 'classnames';

import { getWithAuth } from '../../../../services/generalApiServices';
import Avatar from '../../../Avatar';
import Blank from './Blank';
import Conversation from './Conversation';
import sitemap from '../../../../routing/siteMap';
import { CONFIG } from '../../../../constants';
import './Message.scss';

const mapStateToProps = ({ user }) => ({ user });

class Message extends Component {
  state = {
    conversations: [],
    isShowingConversation: false
  };

  componentDidMount() {
    getWithAuth(
      CONFIG.GET_CONVERSATIONS,
      conversations => this.setState({ conversations }),
      e => console.log(e)
    );
  }

  handleShowConversation = () => {
    this.setState({
      isShowingConversation: true
    });
  }

  handleHideConversation = () => {
    this.setState({
      isShowingConversation: false
    });
  }

  render() {
    const { conversations, isShowingConversation } = this.state;
    const { user } = this.props;
    const activeParticipants = [];
    _.each(conversations, (conversation, i) => {
      let unseen = false;
      if (!conversation.seen.includes(user.id)) {
        unseen = true;
      }
      const activeParticipant = _.filter(conversation.participants, (participant, index) => {
        if (participant._id !== user.id) {
          const template = (
            <div
              key={`active-conversation-${i}-${index}`}
              className={`commonUserItem ${unseen ? 'unseen' : ''}`}
            >
              <Link to={generatePath(sitemap.message, { to: participant.slug })} role="button" onClick={this.handleShowConversation}>
                <div className="innerItem">
                  <Avatar imgPath={`${CONFIG.BUCKET_URL}/users/${participant.picture}`}/>
                </div>
                <div className="innerItem">
                  <ul>
                    <li><span>{participant.name}</span></li>
                    <li>{moment(conversation.updated).calendar()}</li>
                  </ul>
                </div>
              </Link>
            </div>
          );
          activeParticipants.push(template);
        }
        return false;
      });
      activeParticipants.push(...activeParticipant);
    });

    const messageBoxClasses = classnames('messageGridItem', 'messageBox', {
      'box-showing': isShowingConversation
    });

    return (
      <div id="messagePage">
        <section className="messageSection">
          <div className="customContainer mediumWidthLayout">
            <div className="sectionContent">
              <div className="messageGridItem messageListBox">
                <div className="title">
                  <h5>Messages</h5>
                </div>
                <div className="content">
                  <div className="commonUserContainer typeLargeIcon">
                    {activeParticipants}
                  </div>
                </div>
              </div>

              <div className={messageBoxClasses}>
                <Switch>
                  <Route exact path={sitemap.messages} component={Blank}/>
                  <Route exact path={sitemap.message} render={props => <Conversation {...props} close={this.handleHideConversation} showConversation={this.handleShowConversation}/>}/>
                </Switch>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

Message.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(Message);
