import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import Avatar from '../../../Avatar';
import Loading from '../../../Loading';
import store from '../../../../redux';
import { showDialog, hideDialog } from '../../../../redux/actions/dialog.actions';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import { getWithAuth, postAuth } from '../../../../services/generalApiServices';
import { CONFIG } from '../../../../constants';
import CommonButton from '../../../Button';
import './notification.scss';

class Notification extends Component {
  state = {
    notifications: [],
    loading: true
  };

  componentDidMount() {
    this.getNotifications();
  }

  getNotifications = () => {
    getWithAuth(
      CONFIG.GET_NOTIFICATIONS,
      (notifications) => {
        // TODO: LImit from BACKEND
        notifications.splice(100);
        this.setState({
          notifications,
          loading: false
        });
      },
      (e) => {
        console.log(e);
        this.setState({ loading: false });
      }
    );
  }

  clearNotifications = () => {
    postAuth(
      CONFIG.CLEAR_NOTIFICATIONS,
      () => {
        store.dispatch(hideDialog());
        store.dispatch(showGlobalSnack('normal', 'Your notifications have been cleared', 3000));
        this.getNotifications();
      },
      (e) => {
        console.error(e);
        store.dispatch(showGlobalSnack('error', 'Something went wrong. Please reload and try again', 3000));
      }
    );
  }

  clearNotificationsDialog = () => {
    store.dispatch(showDialog('Clear All Notifications?', 'This will delete all your notifications. Are you sure?', this.clearNotifications));
  }

  approveFollowRequest = (notification) => {
    postAuth(
      CONFIG.APPROVE_FOLLOW,
      () => {
        this.setState({ notifications: [], loading: true });
        this.getNotifications();
      },
      error => console.log(error),
      {
        notification
      }
    );
  }

  render() {
    const { notifications, loading } = this.state;
    const notificationList = notifications.map((notification) => {
      if (notification.type === 'follow-request') {
        return (
          <div className="commonUserItem follow-request" key={`notification-${notification._id}`}>
            <div className="innerItem">
              <Avatar imgPath={notification.sender && `${CONFIG.BUCKET_URL}/users/${notification.sender.picture}`}/>
            </div>
            <div className="innerItem _info">
              <ul>
                <li><span>{notification.sender && notification.sender.name}</span>{notification.message}</li>
                <li>{moment(notification.created).fromNow()}</li>
              </ul>
            </div>
            <CommonButton onClick={() => { this.approveFollowRequest(notification); }}>Approve</CommonButton>
          </div>
        );
      }
      return (
        <div className="commonUserItem" key={`notification-${notification._id}`}>
          <Link to={notification.link}>
            <div className="innerItem">
              <Avatar imgPath={notification.sender && `${CONFIG.BUCKET_URL}/users/${notification.sender.picture}`}/>
            </div>
            <div className="innerItem">
              <ul>
                <li><span>{notification.sender && notification.sender.name}</span>{notification.message}</li>
                <li>{moment(notification.created).fromNow()}</li>
              </ul>
            </div>
          </Link>
        </div>
      );
    });
    return (
      <div id="notificationPage">
        <section className="notificationSection">
          <div className="customContainer smallWidthLayout">
            <div className="sectionContent addFullHeight">
              <div className="title">
                <div className="titleItem">
                  <h5>Your Notifications</h5>
                </div>
                {
                  notifications.length > 0 && (
                    <div className="titleItem">
                      <button onClick={this.clearNotificationsDialog}>Clear All Notifcations</button>
                    </div>
                  )
                }
              </div>
              <div className="content">
                {
                  loading
                    ? (
                      <Loading color="red"/>
                    )
                    : (
                      <div className="commonUserContainer">
                        {notificationList}
                      </div>
                    )
                }
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Notification;
