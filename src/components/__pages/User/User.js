import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import sitemap from '../../../routing/siteMap';
import Message from './Message';
import Notification from './Notification';
import Profile from './Profile';
import Upload from './Upload';
import Edit from './Edit';
import GetStudio from './GetStudio';
import ArtistCode from './ArtistCode';
import { Checkout, Success, Failure } from './Cart';
import Loading from '../../Loading';

const mapStateToProps = state => ({
  user: state.user
});

class User extends Component {
  componentDidUpdate(preVprops) {
    const { history, user, isUserRefreshInProgress } = this.props;
    if (preVprops.isUserRefreshInProgress !== isUserRefreshInProgress) {
      if (!isUserRefreshInProgress && isEmpty(user)) {
        history.push(sitemap.login);
      }
    }
  }

  render() {
    const { user, isUserRefreshInProgress } = this.props;
    return (
      <>
        {
          isUserRefreshInProgress
            ? <Loading color="red"/>
            : (
              <>
                {
                  user
                    ? (
                      <Switch>
                        <Route path={sitemap.messages} render={props => <Message {...props} user={user}/>}/>
                        <Route exact path={sitemap.profile} render={props => <Profile {...props} user={user}/>}/>
                        <Route exact path={sitemap.notifications} render={props => <Notification {...props} user={user}/>}/>
                        <Route exact path={sitemap.getStudio} render={props => <GetStudio {...props} user={user}/>}/>
                        <Route exact path={sitemap.artistCode} render={props => <ArtistCode {...props} user={user}/>}/>
                        <Route exact path={sitemap.uploadArt} render={props => <Upload {...props} user={user}/>}/>
                        <Route exact path={sitemap.editInformation} render={props => <Edit {...props} user={user}/>}/>
                        <Route exact path={sitemap.checkout} render={props => <Checkout {...props} user={user}/>}/>
                        <Route exact path={sitemap.checkoutSuccess} render={props => <Success {...props} user={user}/>}/>
                        <Route exact path={sitemap.checkoutFailure} render={props => <Failure {...props} user={user}/>}/>
                        <Redirect to={sitemap.profile}/>
                      </Switch>
                    )
                    : (
                      <Switch>
                        <Redirect to="/"/>
                      </Switch>
                    )
                }
              </>
            )
        }
      </>
    );
  }
}

User.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  loggingIn: PropTypes.bool.isRequired,
  isUserRefreshInProgress: PropTypes.bool.isRequired
};

User.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(User);
