import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import { generatePath } from 'react-router';
import classnames from 'classnames';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/core/styles';

import Header from '../../../UserPageHeader';
import ContentCard from '../../../ContentCard';
import { Input } from '../../../Form';
import { get, postAuthMultipart } from '../../../../services/generalApiServices';
import { CONFIG } from '../../../../constants';
import store from '../../../../redux';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import Loading from '../../../Loading';
import Button from '../../../Button';
import { CoverUploader, ProfilePictureUploader } from '../../../ImageUploader';
import { dataURItoBlob } from '../../../../utils/mediaUtils';
import FancyNav from '../../../FancyNav';
import sitemap from '../../../../routing/siteMap';

import './Edit.scss';

const styles = {
  root: {
    width: '100%',
    margin: 0
  },
  label: {
    flexGrow: 1,
    fontFamily: '"Muli", sans-serif'
  }
};

class Edit extends Component {
  constructor() {
    super();
    this.state = {
      allCategories: [],
      mainCategory: null,
      categories: [],
      errors: {},
      picture: {
        url: null,
        dataUrl: null,
        file: null
      },
      cover: {
        url: null,
        dataUrl: null,
        file: null
      },
      fullName: '',
      dob: moment(),
      email: '',
      address: '',
      bio: '',
      achievements: '',
      loadingUser: true,
      saving: false,
      privateProfile: null
    };
    this.pickerDialogRef = React.createRef();
  }

  componentDidMount() {
    this.getUserInfo();
    get(
      CONFIG.GET_CATEGORIES_SORTED,
      (allCategories) => {
        this.setState({
          allCategories
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }

  handleCategorySelect = (categoryId) => {
    this.setState({ mainCategory: categoryId });
  }

  handleSubCategoryClick = (categoryId) => {
    const { categories } = this.state;
    const newCategories = [...categories];
    if (categories.indexOf(categoryId) < 0) {
      newCategories.push(categoryId);
    } else {
      newCategories.splice(newCategories.indexOf(categoryId), 1);
    }
    this.setState({ categories: newCategories });
  }

  handlePrivateProfile = (e, toggle) => {
    this.setState({ privateProfile: toggle });
  }

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  handleDateChange = (date) => {
    this.setState({ dob: date });
  };

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  isUserArtist = () => {
    const { user } = this.props;
    return user.role > 10 && user.role < 21;
  }

  getUserInfo = () => {
    const { user } = this.props;
    this.setState({ loadingUser: true }, () => {
      get(
        generatePath(CONFIG.GET_USER_BY_ID, { id: user.id }),
        (res) => {
          this.setState({
            loadingUser: false,
            picture: {
              url: res.picture
            },
            cover: {
              url: res.cover
            },
            fullName: res.name,
            dob: moment(res.info.dob),
            email: res.email,
            address: res.info.address,
            bio: res.info.biography,
            achievements: res.info.achievements,
            categories: res.info.categories,
            privateProfile: res.settings && res.settings.private
          });
        },
        (e) => {
          console.log(e);
          store.dispatch(showGlobalSnack('error', 'Something went wrong. Please try again.', 3000));
        }
      );
    });
  }

  handleCroppedCoverImage = (dataUrl) => {
    const { cover } = this.state;
    const newCover = { ...cover };
    newCover.dataUrl = dataUrl;
    this.setState({
      cover: newCover
    });
  }

  handleCoverImageChange = (e) => {
    const { cover } = this.state;
    const newCover = { ...cover };
    newCover.file = e.target.files[0]; // eslint-disable-line
    this.setState({
      cover: newCover
    });
  }

  handleCroppedPP = (dataUrl) => {
    const { picture } = this.state;
    const newPicture = { ...picture };
    newPicture.dataUrl = dataUrl;
    this.setState({
      picture: newPicture
    });
  }

  handleCoverPPChange = (e) => {
    const { picture } = this.state;
    const newPicture = { ...picture };
    newPicture.file = e.target.files[0]; // eslint-disable-line
    this.setState({
      picture: newPicture
    });
  }

  handleRemovePP = () => {
    this.setState({
      picture: {
        url: null,
        dataUrl: null,
        file: null
      }
    });
  };

  handleCancel = () => {
    const { history, user } = this.props;

    if (user.role > 10 && user.role < 21) {
      history.push(generatePath(sitemap.artist, { slug: user.slug }));
    } else {
      history.push(sitemap.user);
    }
  };

  handleSubmit = () => {
    const data = new FormData();
    const {
      cover,
      fullName,
      dob,
      email,
      address,
      bio,
      achievements,
      allCategories,
      categories,
      picture,
      privateProfile
    } = this.state;

    if (cover.dataUrl) {
      data.append('cover', dataURItoBlob(cover.dataUrl));
    } else if (cover.url) {
      data.append('cover', cover.url);
    }

    if (picture.dataUrl) {
      data.append('picture', dataURItoBlob(picture.dataUrl));
    } else if (picture.url) {
      data.append('picture', picture.url);
    }

    data.append('name', fullName);
    data.append('email', email);
    data.append('info.address', address);
    data.append('info.dob', dob.format('MM/DD/YYYY'));
    data.append('info.biography', bio);
    data.append('info.achievements', achievements);
    data.append('settings.private', privateProfile);

    // categories
    const allCategoriesTemp = [];
    categories.forEach((categoryId) => {
      const categoryObj = allCategories.find(category => category._id === categoryId);
      if (categoryObj.parent && allCategoriesTemp.indexOf(categoryObj.parent._id) < 0) {
        allCategoriesTemp.push(categoryObj.parent._id);
      }
    });
    allCategoriesTemp.push(...categories);
    for (let i = 0; i < allCategoriesTemp.length; i++) {
      data.append('info.categories[]', allCategoriesTemp[i]);
    }

    this.setState({ saving: true });

    postAuthMultipart(
      CONFIG.UPDATE_USER_INFO,
      () => {
        this.setState({ saving: false });
        window.location.reload();
      },
      (e) => {
        console.log(e);
        store.dispatch(showGlobalSnack('error', 'Something went wrong. Please try again.', 3000));
      },
      data
    );
  }

  render() {
    const {
      allCategories,
      categories,
      picture,
      cover,
      errors,
      loadingUser,
      fullName,
      dob,
      email,
      address,
      bio,
      achievements,
      saving,
      privateProfile
    } = this.state;

    let { mainCategory } = this.state;

    const { classes } = this.props;

    // first paint main category
    if (!mainCategory && categories.length > 0 && allCategories.length > 0) {
      for (let i = 0; i < categories.length; i++) {
        const categoryId = categories[i];
        const catObj = allCategories.find(category => category._id === categoryId);
        if (catObj && !catObj.parent) {
          mainCategory = categoryId;
          break;
        }
      }
    }

    const topCategories = allCategories.filter(category => !category.parent);
    const categoryNavList = topCategories.map(category => (
      {
        name: category.name,
        key: category._id
      }
    ));
    const categoryChildren = allCategories.filter(category => category.parent && category.parent._id === mainCategory);
    const categoryChildrenList = categoryChildren.map((category) => {
      const buttonClasses = classnames('category-select', {
        selected: categories.indexOf(category._id) > -1
      });
      return (
        <button
          key={`category-select-${category._id}`}
          className={buttonClasses}
          onClick={() => { this.handleSubCategoryClick(category._id); }}
        >
          {category.name}
        </button>
      );
    });

    return (
      <div id="edit-info-page">
        <Header
          title="Edit Information"
          subTitle="Add your information to be more visible"
          loading={saving}
          cancel={this.handleCancel}
          publish={this.handleSubmit}
        />

        <section className="commonFormSection">
          {
            loadingUser
              ? <Loading color="red"/>
              : (
                <div className="customContainer smallWidthLayout">
                  <ContentCard title="Profile Picture">
                    <div className="profile-picture">
                      {
                        picture.url
                          ? (
                            <div className="pp">
                              <img src={`${CONFIG.BUCKET_URL}/users/${picture.url}`} alt=""/>
                              <Button
                                mode="special"
                                size="auto"
                                onClick={() => { this.setState({ picture: { url: null, dataUrl: null, file: null } }); }}
                              >
                                Change Image
                              </Button>
                            </div>
                          )
                          : (
                            <ProfilePictureUploader
                              setCroppedImage={this.handleCroppedPP}
                              onImageChange={this.handleCoverPPChange}
                              imageFile={picture.file}
                              width={200}
                              height={200}
                              remove={this.handleRemovePP}
                            />
                          )
                      }
                    </div>
                  </ContentCard>
                  {
                    this.isUserArtist()
                    && (
                      <>
                        <ContentCard title="Cover Image">
                          <div className="cover-image">
                            {
                              cover.url
                                ? (
                                  <img src={`${CONFIG.BUCKET_URL}/users/${cover.url}`} alt={fullName}/>
                                )
                                : (
                                  <CoverUploader
                                    setCroppedImage={this.handleCroppedCoverImage}
                                    onImageChange={this.handleCoverImageChange}
                                    imageFile={cover.file}
                                    width={480}
                                    height={148}
                                  />
                                )
                            }
                            <Button
                              mode="special"
                              size="auto"
                              onClick={() => { this.setState({ cover: { url: null, dataUrl: null, file: null } }); }}
                            >
                              Change Image
                            </Button>
                          </div>
                        </ContentCard>
                        <ContentCard title="Choose your styles">
                          {
                            categoryNavList.length > 0
                              && (
                                <FancyNav
                                  activeItemKey={mainCategory}
                                  items={categoryNavList}
                                  onClick={this.handleCategorySelect}
                                />
                              )
                          }
                          <div className="common-category-list">
                            {categoryChildrenList}
                          </div>
                        </ContentCard>
                      </>
                    )
                  }
                  <ContentCard title="Basic Information">
                    <div className="typeTwoCol">
                      <Input
                        required
                        label="Full Name"
                        type="text"
                        name="fullName"
                        value={fullName}
                        onChange={e => this.setState({ fullName: e.target.value })}
                      />
                      <Input
                        required
                        label="Date of Birth"
                        type="text"
                        name="dob"
                        errors={errors}
                        value={dob.format('Do MMM, YYYY')}
                        onClick={this.openPickerDialog}
                      />
                      <div className="datePickerHide">
                        <MuiPickersUtilsProvider utils={MomentUtils}>
                          <DatePicker
                            ref={this.pickerDialogRef}
                            margin="normal"
                            label="Date Of Birth"
                            value={dob.format('Do MMM, YYYY')}
                            onChange={this.handleDateChange}
                            style={{
                              width: '100%'
                            }}
                          />
                        </MuiPickersUtilsProvider>
                      </div>
                      <Input
                        required
                        label="Email Address"
                        type="email"
                        name="Email"
                        value={email}
                        onChange={e => this.setState({ email: e.target.value })}
                      />
                      <Input
                        required
                        label="Address"
                        type="text"
                        name="address"
                        value={address}
                        onChange={e => this.setState({ address: e.target.value })}
                      />
                    </div>
                  </ContentCard>
                  {
                    this.isUserArtist()
                    && (
                      <ContentCard title="More Details">
                        <Input
                          required
                          label="Biography"
                          type="text"
                          name="biography"
                          value={bio}
                          onChange={e => this.setState({ bio: e.target.value })}
                        />
                      </ContentCard>
                    )
                  }
                  {
                    this.isUserArtist()
                    && (
                      <ContentCard title="More Details">
                        <Input
                          required
                          label="Achievements"
                          type="text"
                          name="achievements"
                          value={achievements}
                          onChange={e => this.setState({ achievements: e.target.value })}
                        />
                      </ContentCard>
                    )
                  }
                  {
                    this.isUserArtist()
                    && (
                      <ContentCard title="Private Profile">
                        <p>You will have to approve requests for followers if your profile is private</p>
                        <FormControlLabel
                          labelPlacement="start"
                          classes={classes}
                          control={
                            (
                              <Switch
                                checked={privateProfile}
                                onChange={this.handlePrivateProfile}
                                value="privateProfile"
                                color="primary"
                              />
                            )
                          }
                          label="Make my profile private"
                        />
                      </ContentCard>
                    )
                  }
                </div>
              )
          }
        </section>
      </div>
    );
  }
}

Edit.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

export default withStyles(styles)(Edit);
