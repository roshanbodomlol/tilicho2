import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import RemoveIcon from '@material-ui/icons/RemoveCircleRounded';
import IconButton from '@material-ui/core/IconButton';

import CommonButton from '../../../Button';
import { postAuth, post } from '../../../../services/generalApiServices';
import { CONFIG } from '../../../../constants';
import store from '../../../../redux';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import { showDialog, hideDialog } from '../../../../redux/actions/dialog.actions';
import { removeCartItem } from '../../../../redux/actions/cart.actions';
import Loading from '../../../Loading/Loading';
import { getThumbnail } from '../../../../utils/mediaUtils';

const mapStateToProps = ({ user, cart }) => ({ user, cart });

class Checkout extends Component {
  state = {
    isSaving: false,
    amount: 0,
    taxAmount: 0,
    serviceCharge: 0,
    deliveryCharge: 0,
    merchantId: null,
    gatewayUrl: null,
    items: [],
    isLoading: true
  }

  formValid = false;

  formRef = React.createRef();

  componentDidMount() {
    this.populateItems();
  }

  populateItems = () => {
    const { cart } = this.props;
    const itemIds = cart.items.map(item => item.productId);
    post(
      CONFIG.GET_PRODUCTS_BY_IDS,
      products => this.setState({ items: products, isLoading: false }),
      (e) => {
        console.log(e);
        store.dispatch(showGlobalSnack('error', 'An unexpected error has occurred. Please reload the page'));
      },
      {
        items: itemIds
      }
    );
  }

  removeItem = (id) => {
    store.dispatch(showDialog('Remove Item', 'Are you sure you want to remove this item?', () => {
      store.dispatch(removeCartItem(id));
      store.dispatch(hideDialog());
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { user } = this.props;
    // Prepare order
    const demoData = {
      user: user.id,
      items: [
        {
          product: '5c653c160d0a854449215c71'
        }
      ]
    };

    // place order
    store.dispatch(showGlobalSnack('normal', 'You will shortly be redirected to payment gateway. Please wait...'));
    this.setState({ isSaving: true });
    postAuth(
      CONFIG.PLACE_ORDER,
      ({ amount, taxAmount, deliveryCharge, serviceCharge, merchantId, gatewayUrl, pid }) => {
        this.setState({
          amount,
          taxAmount,
          deliveryCharge,
          serviceCharge,
          merchantId,
          gatewayUrl,
          pid
        }, () => {
          // redirect to payment gateway
          this.formRef.current.submit();
        });
      },
      (err) => {
        console.log(err);
        store.dispatch(showGlobalSnack('error', 'An unexpected error has occurred. Please reload and try again.'));
      },
      demoData
    );
  }

  render() {
    const {
      isSaving,
      amount,
      taxAmount,
      deliveryCharge,
      serviceCharge,
      merchantId,
      gatewayUrl,
      pid,
      items,
      isLoading
    } = this.state;

    const itemsList = items.map((item, index) => (
      <tr key={`checkout-product-${item._id}`}>
        <td>{index}</td>
        <td>
          <img src={getThumbnail(item.image, 'thumbnail').url} alt=""/>
        </td>
        <td>{item.name}</td>
        <td>{item.priceField}</td>
        <td>
          <IconButton onClick={() => { this.removeItem(item._id); }}>
            <RemoveIcon/>
          </IconButton>
        </td>
      </tr>
    ));

    return (
      <div className="checkout-page">
        {
          isLoading
            ? <Loading color="red"/>
            : (
              <>
                {
                  items.length > 0
                    ? (
                      <>
                        <div className="items">
                          <table>
                            {itemsList}
                          </table>
                        </div>
                        <div className="checkout-form">
                          <form ref={this.formRef} action={gatewayUrl} method="POST" onSubmit={this.handleSubmit}>
                            <input value={amount + taxAmount + deliveryCharge + serviceCharge} name="tAmt" type="hidden"/>
                            <input value={amount} name="amt" type="hidden"/>
                            <input value={taxAmount} name="txAmt" type="hidden"/>
                            <input value={serviceCharge} name="psc" type="hidden"/>
                            <input value={deliveryCharge} name="pdc" type="hidden"/>
                            <input value={merchantId} name="scd" type="hidden"/>
                            <input value={pid} name="pid" type="hidden"/>
                            <input value="https://localhost:3000/user/cart/checkout/success" type="hidden" name="su"/>
                            <input value="https://localhost:3000/user/cart/checkout/failure" type="hidden" name="fu"/>
                            <CommonButton type="submit" loading={isSaving}>Place Order</CommonButton>
                          </form>
                        </div>
                      </>
                    )
                    : 'You don\'t have any items in your cart'
                }
              </>
            )
        }
      </div>
    );
  }
}

Checkout.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  cart: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(Checkout);
