import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from 'axios';
import Cookie from 'js-cookie';

import store from '../../../../redux';
import sitemap from '../../../../routing/siteMap';
import AdvancedAvatar from '../../../AdvancedAvatar/AdvancedAvatar';
import CommonButton from '../../../Button';
import ContentCard from '../../../ContentCard';
import ImageUploader from '../../../ImageUploader';
import { Input } from '../../../Form';
import { showGlobalSnack } from '../../../../redux/actions/snack.actions';
import { CONFIG } from '../../../../constants';

import './GetStudio.scss';

class GetStudio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      aboutYourself: '',
      submitting: false
    };
  }

  handleAddImage = (file, dataUrl, id) => {
    const { images } = this.state;
    const newImagesState = [...images];
    newImagesState.push({
      id,
      file,
      dataUrl
    });
    this.setState({ images: newImagesState });
  }

  handleRemoveImage = (imageId) => {
    const { images } = this.state;
    const newImages = [...images];
    const imageIdIndex = newImages.findIndex(image => image.id === imageId);
    newImages.splice(imageIdIndex, 1);
    this.setState({
      images: newImages
    });
  }

  handleSubmit = () => {
    const { history } = this.props;
    const { images, aboutYourself } = this.state;

    this.setState({ submitting: true });

    const token = Cookie.get(CONFIG.AUTH_TOKEN);

    const data = new FormData();
    for (let i = 0; i < images.length; i++) {
      data.append('images', images[i].file);
    }
    data.append('message', aboutYourself);

    if (token) {
      axios({
        url: CONFIG.GET_STUDIO,
        method: 'POST',
        data,
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `JWT ${token}`
        }
      })
        .then((response) => {
          if (response.data.status === 'success') {
            store.dispatch(showGlobalSnack('normal', 'Your application has been received. We will contact you as soon as possible.', 3000));
            history.push(sitemap.user);
          } else if (response.data.status === 'already-exists') {
            this.setState({
              submitting: false
            });
            store.dispatch(showGlobalSnack('error', 'You have already submitted your application.', 3000));
            history.push(sitemap.user);
          } else {
            throw new Error(response.data.message);
          }
        })
        .catch((e) => {
          console.error(e);
          store.dispatch(showGlobalSnack('error', 'An unexpected error has occurred. Please try again', 3000));
        });
    }
  };

  render() {
    // const {
    //   artistCode,
    //   description
    // } = this.state;
    const {
      images,
      aboutYourself,
      submitting
    } = this.state;
    return (
      <div id="getStudio-page">
        <div className="commonGidContainer typeBox">
          <div className="gridItem artistSideBar">
            <div className="inner">
              <div className="sideBarGroup profileSideBarGroup">
                <div className="sideGroupContent">
                  <div className="sideBarItem">
                    <AdvancedAvatar/>
                    <div className="action">
                      <ul>
                        <li>
                          <NavLink to={sitemap.editInformation} activeClassName="active">
                            <CommonButton size="medium" mode="specialBlack">Edit Profile</CommonButton>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="gridItem mainContent">
            <section className="getStudioSection removeMarginBottom">
              <div className="customContainer smallWidthLayout">
                <div className="sectionContent addFullHeight">
                  <div className="sectionTitle">
                    <h2>Request for a Studio</h2>
                    <p>Upload and Send us your artwork or pictures to get a studio. Upload images in .jpg and .png format each images less then 2 MB.</p>
                  </div>

                  <div className="formContainer">
                    <div className="fields">
                      <ContentCard title="Upload Images">
                        <ImageUploader
                          images={images}
                          addImage={this.handleAddImage}
                          removeImage={this.handleRemoveImage}
                          coverImage={false}
                        />
                      </ContentCard>
                      <ContentCard title="About Yourself">
                        <Input
                          required
                          label="Description"
                          type="textarea"
                          name="description"
                          value={aboutYourself}
                          placeholder="Write a short description about yourself. You can also add link to your portfolio."
                          onChange={(e) => { this.setState({ aboutYourself: e.target.value }); }}
                        />
                      </ContentCard>
                    </div>

                    <div className="formAction">
                      <div className="actionItem">
                        <NavLink to={sitemap.artistCode} activeClassName="active">
                          Have a Artist Code?
                        </NavLink>
                      </div>
                      <div className="actionItem">
                        <CommonButton size="buttonAuto" mode="plain">Cancel</CommonButton>
                        <CommonButton loading={submitting} size="buttonAuto" onClick={this.handleSubmit}>Send A Request</CommonButton>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

GetStudio.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired
};

export default GetStudio;
