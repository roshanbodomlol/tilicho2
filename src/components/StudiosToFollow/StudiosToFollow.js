import React, { Component } from 'react';
import { injectIntl, intlShape } from 'react-intl';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import HomeSection from '../HomeSection';
import ContentScroller from '../ContentScroller';
import Loading from '../Loading';
import ImageContainer from '../ImageContainer';
import FollowArtist from '../FollowArtist';
import sitemap from '../../routing/siteMap';
import { getStudiosToFollow } from '../../services/studioServices';
import { CONFIG } from '../../constants';
import './StudiosToFollow.scss';

class StudiosToFollow extends Component {
  state = {
    isLoading: true,
    studios: []
  }

  componentDidMount() {
    this.getStudiosToFollow();
  }

  getStudiosToFollow = () => {
    getStudiosToFollow()
      .then((studios) => {
        this.setState({ isLoading: false, studios });
      })
      .catch((e) => {
        console.log('Error while trying to get studios to follow', e);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { studios, isLoading } = this.state;
    const { intl } = this.props;

    const studiosList = studios.map((studio) => {
      const detailsArray = [];
      if (studio.follows && studio.follows.length > 0) detailsArray.push(intl.formatMessage({ id: 'ui.artist.followers' }, { count: studio.follows.length }));
      if (studio.products && studio.products.length > 0) detailsArray.push(intl.formatMessage({ id: 'ui.artist.works' }, { count: studio.products.length }));
      const detailsString = detailsArray.join(' / ');
      return (
        <div key={`studio-follow-${studio.id}`} className="studio">
          <Link to={generatePath(sitemap.artist, { slug: studio.slug })}>
            <ImageContainer
              imgPath={`${CONFIG.BUCKET_URL}/users/${studio.picture}`}
              width={300}
              height={250}
              size="cover"
              borderRadius={7}
            />
          </Link>
          <div className="details">
            <div className="left">
              <div className="name">{studio.name}</div>
              <div className="more-details">{detailsString}</div>
            </div>
            <div className="right">
              <FollowArtist artist={studio}/>
            </div>
          </div>
        </div>
      );
    });
    return (
      <div className="studios-to-follow">
        <HomeSection title="Studios to follow" fullWidth>
          {
            isLoading
              ? <Loading/>
              : (
                <ContentScroller
                  arrowPosition="around"
                  studiosToFollow
                >
                  {studiosList}
                </ContentScroller>
              )
          }
        </HomeSection>
      </div>
    );
  }
}

StudiosToFollow.propTypes = {
  intl: intlShape.isRequired
};

export default injectIntl(StudiosToFollow);
