import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { objectOf, any, arrayOf } from 'prop-types';
import CartIcon from '@material-ui/icons/ShoppingCartRounded';

import store from '../../redux';
import { showGlobalSnack } from '../../redux/actions/snack.actions';
import { addToCart } from '../../redux/actions/cart.actions';
import siteMap from '../../routing/siteMap';
import './AddToCart.scss';

const mapStateToProps = ({ user, cart }) => ({ user, cart });

class AddToCart extends Component {
  handleAddToCart = () => {
    const { user, cart, history, art } = this.props;
    if (user) {
      // check if item already in cart
      if (cart.items.findIndex(item => item.productId === art.id) < 0) {
        // add to cart
        store.dispatch(addToCart(art.id, 1));
        store.dispatch(showGlobalSnack('normal', `Item ${art.name} added to cart`, 3000));
      } else {
        store.dispatch(showGlobalSnack('normal', 'This item is already in your cart', 3000));
      }
    } else {
      history.push(siteMap.login);
      store.dispatch(showGlobalSnack('normal', 'Please login to continue', 3000));
    }
  }

  render() {
    return (
      <span
        className="add-to-cart-button"
        role="button"
        tabIndex="-1"
        onClick={this.handleAddToCart}
      >
        <CartIcon style={{ color: '#000' }}/>
      </span>
    );
  }
}

AddToCart.propTypes = {
  user: objectOf(any).isRequired,
  cart: arrayOf(any).isRequired,
  history: objectOf(any).isRequired,
  art: objectOf(any).isRequired
};

export default connect(mapStateToProps)(withRouter(AddToCart));
