import React, { Component } from 'react';
import { string } from 'prop-types';
import { generatePath } from 'react-router';
import { Link } from 'react-router-dom';

import { getPopularProductsByCategory } from '../../services/productServices';
import Loading from '../Loading';
import HomeSection from '../HomeSection';
import ContentScroller from '../ContentScroller';
import { getThumbnail } from '../../utils/mediaUtils';
import siteMap from '../../routing/siteMap';
import { CONFIG } from '../../constants';
import './ArtList.scss';

class ArtList extends Component {
  state = {
    isLoading: false,
    items: []
  };

  componentDidMount() {
    const { categoryId, name } = this.props;

    getPopularProductsByCategory(categoryId, (items) => {
      this.setState({ items, isLoading: false });
    }, (err) => {
      console.log(`Error occurred while trying to fetch ${name}`, err);
    });
  }

  render() {
    const { name } = this.props;
    const { items, isLoading } = this.state;

    const artList = items.map((item) => {
      const thumbnail = getThumbnail(item.image, 'productImage');
      return (
        <div key={`${name}-item-${item._id}`} className="popularItem">
          <Link to={generatePath(siteMap.artWork, { slug: item.slug })}>
            <img src={`${CONFIG.BUCKET_URL}/${item.image.set.slug}/${thumbnail.fileName}`} alt=""/>
          </Link>
        </div>
      );
    });

    return (
      <div className="art-list">
        <HomeSection title={name} fullWidth>
          {
            isLoading
              ? <Loading color="red"/>
              : (
                <ContentScroller
                  title={name}
                  arrowPosition="around"
                >
                  {artList}
                </ContentScroller>
              )
          }
        </HomeSection>
      </div>
    );
  }
}

ArtList.propTypes = {
  categoryId: string.isRequired,
  name: string.isRequired
};

export default ArtList;
