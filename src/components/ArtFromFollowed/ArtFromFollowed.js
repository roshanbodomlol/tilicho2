import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import { getProductsFromFollowed } from '../../services/productServices';
import { getThumbnail } from '../../utils/mediaUtils';
import { CONFIG } from '../../constants';
import sitemap from '../../routing/siteMap';
import Loading from '../Loading';
import HomeSection from '../HomeSection';
import ContentScroller from '../ContentScroller';
import './ArtFromFollowed.scss';

class ArtFromFollowed extends Component {
  state = {
    artworks: [],
    isLoading: true
  };

  componentDidMount() {
    getProductsFromFollowed()
      .then(artworks => this.setState({ artworks, isLoading: false }))
      .catch((e) => {
        console.log(e);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { artworks, isLoading } = this.state;

    const items = artworks.map((item) => {
      const thumbnail = getThumbnail(item.image, 'productImage');
      return (
        <div key={`popular-item-${item._id}`} className="popularItem">
          <Link to={generatePath(sitemap.artWork, { slug: item.slug })}>
            <img src={`${CONFIG.BUCKET_URL}/${item.image.set.slug}/${thumbnail.fileName}`} alt=""/>
          </Link>
        </div>
      );
    });
    return (
      <HomeSection title="From artists you follow" fullWidth>
        <div className="art-from-followed">
          {
            isLoading
              ? <Loading color="red"/>
              : (
                <ContentScroller
                  title="Popular Works"
                  arrowPosition="around"
                >
                  {items}
                </ContentScroller>
              )
          }
        </div>
      </HomeSection>
    );
  }
}

export default ArtFromFollowed;
