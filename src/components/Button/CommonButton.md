```js
<div style={{
  display: 'flex',
  justifyContent: 'space-around'
}}>
<CommonButton
  size="medium"
  mode="primary"
  loading
>
Follow</CommonButton>
<CommonButton
  size="medium"
  mode="secondary"
  onClick={() => {alert('click!')}}
>
Follow</CommonButton>
<CommonButton
  size="large"
  mode="special"
>
Follow</CommonButton>
</div>
```
