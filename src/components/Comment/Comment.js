import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { generatePath } from 'react-router';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import DownArrow from '@material-ui/icons/KeyboardArrowDown';

import sitemap from '../../routing/siteMap';
import Avatar from '../Avatar';
import Loading from '../Loading';
import { get, postAuth } from '../../services/generalApiServices';
import { CONFIG } from '../../constants';
import './Comment.scss';

const mapStateToProps = state => ({
  user: state.user
});

class Comment extends Component {
  state = {
    comment: '',
    comments: [],
    commenting: false,
    total: CONFIG.FEED_COMMENTS_LIMIT,
    loadingComments: true,
    isCommmentsTruncated: true
  }

  componentDidMount() {
    this.getComments();
  }

  getComments = () => {
    const { item } = this.props;
    const { total } = this.state;
    get(
      generatePath(CONFIG.GET_POST_COMMENTS, { postId: item._id, total }),
      ((post) => {
        this.setState({ comments: post.comments, loadingComments: false });
      })
    );
  }

  handleNewComment = (e) => {
    e.preventDefault();
    const { comment } = this.state;
    const { item } = this.props;
    this.setState({ commenting: true });
    postAuth(
      generatePath(CONFIG.MAKE_POST_COMMENT, { postId: item._id }),
      () => {
        this.setState({ commenting: false, comment: '' });
        this.getComments();
      },
      (err) => {
        console.error(err);
        this.setState({ commenting: false, comment: '' });
      },
      {
        comment,
        post: item._id
      }
    );
  }

  showAllComments = () => {
    this.setState({
      total: 0
    });
  }

  render() {
    const { user } = this.props;
    const {
      comments,
      comment,
      commenting,
      loadingComments,
      total,
      isCommmentsTruncated
    } = this.state;
    const commentsNewFirst = comments.sort((a, b) => {
      return new Date(b.created) - new Date(a.created);
    });
    const truncatedComments = [];
    commentsNewFirst.forEach((commentItem, i) => {
      if (total === 0 || i < total) {
        truncatedComments.push(commentItem);
      }
    });
    const commentsList = truncatedComments.map(commentItem => (
      <div className="commonCommentItem" key={`post-comment-${commentItem._id}`}>
        <Avatar imgPath={`${CONFIG.BUCKET_URL}/users/${commentItem.author && commentItem.author.picture}`}/>
        <div className="commentField">
          <div className="inner">
            {commentItem.comment}
          </div>
        </div>
      </div>
    ));
    return (
      <div className="comments-wrap">
        {
          user
            ? (
              <div className="comments newComments">
                <div className="commonCommentContainer">
                  <div className="commonCommentItem">
                    <Avatar
                      imgPath={`${CONFIG.BUCKET_URL}/users/${user.picture}`}
                    />
                    <div className="commentField">
                      <div className="inner">
                        <form onSubmit={(e) => { this.handleNewComment(e); }}>
                          <input
                            type="text"
                            placeholder="Write a comment..."
                            value={comment}
                            disabled={commenting}
                            onChange={(e) => { this.setState({ comment: e.target.value }); }}
                          />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )
            : (
              <div className="not-logged-in-comment">
                <Link to={sitemap.login}>Write a comment</Link>
              </div>
            )
        }
        <div className="comments">
          <div className="commonCommentContainer">
            {
              loadingComments
                ? <Loading color="red"/>
                : commentsList
            }
          </div>
          {
            isCommmentsTruncated && comments.length > truncatedComments.length
              ? (
                <div className="commentsViewMore" onClick={this.showAllComments} role="button" tabIndex="-1">
                  <p className="coSmallText">View More Comments <DownArrow/></p>
                </div>
              )
              : ''
          }
        </div>
      </div>
    );
  }
}

Comment.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  item: PropTypes.objectOf(PropTypes.any).isRequired
};

Comment.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(Comment);
