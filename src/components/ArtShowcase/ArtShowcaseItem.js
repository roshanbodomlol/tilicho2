import React from 'react';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazy-load';
import { connect } from 'react-redux';
import EditIcon from '@material-ui/icons/Brush';

import sitemap from '../../routing/siteMap';

const mapStateToProps = ({ user }) => ({ user });

const ArtShowcaseItem = ({ artist, thumbnail, artName, price, slug, readOnly, user, artistId, artistSlug }) => {
  const thumbnailRatio = thumbnail.width / thumbnail.height;
  const paddingBottom = (1 / thumbnailRatio) * 100;
  return (
    <div className="artItem">
      <div className="inner">
        {
          !readOnly && user && user.id === artistId && (
            <Link to={generatePath(sitemap.artWorkEdit, { slug })}>
              <button type="button" className="edit-art">
                <EditIcon style={{ fontSize: 10 }}/>
                <span>Edit</span>
              </button>
            </Link>
          )
        }
        <div className="imageContainer">
          <Link to={generatePath(sitemap.artWork, { slug })}>
            <div className="lazy-placeholder" style={{ paddingBottom: `${paddingBottom}%` }}>
              <LazyLoad>
                <img src={thumbnail.url} alt=""/>
              </LazyLoad>
            </div>
          </Link>
        </div>
        <div className="textContainer">
          <div className="textItem">
            <h5>{artName}</h5>
            {
              artistSlug && <span>by <Link to={generatePath(sitemap.artist, { slug: artistSlug })}>{artist}</Link></span>
            }
          </div>
          <div className="textItem">
            <h5>{price}</h5>
          </div>
        </div>
      </div>
    </div>
  );
};

ArtShowcaseItem.propTypes = {
  artist: PropTypes.string.isRequired,
  artName: PropTypes.string.isRequired,
  thumbnail: PropTypes.objectOf(PropTypes.any).isRequired,
  price: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  artistId: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  user: PropTypes.objectOf(PropTypes.any),
  artistSlug: PropTypes.string
};

ArtShowcaseItem.defaultProps = {
  readOnly: true,
  user: null,
  artistSlug: null
};

export default connect(mapStateToProps)(ArtShowcaseItem);
