import React from 'react';
import PropTypes from 'prop-types';
// import classnames from 'classnames';

import './ContentCard.scss';

// const ContentCard = ({ title, children, className }) => (
const ContentCard = ({ title, subtitle, children }) => (
  // <div className={classnames(styles.contentCardItem, className)}>
  <div className="contentCardItem">
    <div className="title">
      <h5>{title}</h5>
      {
        subtitle && <small>{subtitle}</small>
      }
    </div>
    <div className="main">
      {children}
    </div>
  </div>
);

ContentCard.propTypes = {
  title: PropTypes.string,
  // className: PropTypes.string,
  children: PropTypes.node,
  subtitle: PropTypes.string
};

ContentCard.defaultProps = {
  title: '',
  // className: '',
  children: '',
  subtitle: null
};

export default ContentCard;
