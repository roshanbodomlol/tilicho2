import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import { generatePath } from 'react-router';
import PropTypes from 'prop-types';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { isEmpty } from 'lodash';
// import SearchIcon from '@material-ui/icons/Search';
import BurgerIcon from '@material-ui/icons/Menu';
import DownIcon from '@material-ui/icons/KeyboardArrowDown';

import CircularProgress from '@material-ui/core/CircularProgress';
import userIcon from '../../../../assets/img/user.jpg';

import { CONFIG } from '../../../../constants';
import { userLogout } from '../../../../redux/actions/user.actions';
import sitemap from '../../../../routing/siteMap';
import store from '../../../../redux';
import Logo from '../../../Logo/Logo';
import QuickSearch from '../../../QuickSearch';
import Notifications from '../DesktopHeader/Notifications';
import Messages from '../DesktopHeader/Messages';
import './MobileHeader.scss';

const mapStateToProps = state => (
  {
    user: state.user
  }
);

class MobileHeader extends Component {
  state = {
    drawerOpen: false,
    isLanguageOpen: false
  };

  handleShowDrawer = () => {
    this.setState({
      drawerOpen: true
    });
  }

  handleHideDrawer = () => {
    this.setState({
      drawerOpen: false
    });
  }

  handleShowLanguage = () => {
    this.setState(prevState => ({
      isLanguageOpen: !prevState.isLanguageOpen
    }));
  }

  handleHideLanguage = () => {
    this.setState({
      isLanguageOpen: false
    });
  }

  logOut = () => {
    const { history } = this.props;
    store.dispatch(userLogout());
    history.push('/');
  }

  render() {
    const { user, isUserRefreshInProgress } = this.props;
    const { drawerOpen } = this.state;

    const isUserAvailable = !isEmpty(user);
    return (
      <header id="headerWrapper">
        <div className="headerContainer">
          <div className="toggleContainer">
            <BurgerIcon onClick={this.handleShowDrawer}/>
          </div>

          <div className="logoContainer">
            <Logo/>
          </div>

          <div className="searchContainer">
            <QuickSearch mobile/>
          </div>
        </div>
        <SwipeableDrawer
          anchor="left"
          open={drawerOpen}
          onClose={this.handleHideDrawer}
          onOpen={this.handleShowDrawer}
          PaperProps={{ style: { width: '80%' } }}
        >
          <div className="mobileNavContainer">
            <div className="navButtons">
              {
                user
                  ? (
                    <div className="linkBlock">
                      {/* <span onClick={this.handleHideDrawer} role="button" tabIndex="-1">
                        <Messages/>
                      </span> */}
                      <span onClick={this.handleHideDrawer} role="button" tabIndex="-1">
                        <Notifications/>
                      </span>
                    </div>
                  )
                  : (
                    <div className="buttonBlock">
                      <ul>
                        <li><NavLink to={sitemap.login} activeClassName="active" onClick={this.handleHideDrawer}>Register / Login</NavLink></li>
                      </ul>
                    </div>
                  )
              }
            </div>

            <div className="nav">
              <ul>
                <li><NavLink to="/" onClick={this.handleHideDrawer}>Home</NavLink></li>
                <li><NavLink to="/gallery/all" onClick={this.handleHideDrawer}>Gallery</NavLink></li>
                <li><NavLink to="/studios" onClick={this.handleHideDrawer}>Studios</NavLink></li>
                <li><NavLink to="/feed" onClick={this.handleHideDrawer}>Feed</NavLink></li>
                {
                  isUserAvailable && (
                    <>
                      <li>
                        {
                          isUserRefreshInProgress
                            ? <CircularProgress style={{ width: 24, height: 24 }}/>
                            : (
                              <>
                                <li className="userProfile menu-showing">
                                  <span>
                                    <i>
                                      {
                                        user.picture
                                          ? <img src={`${CONFIG.BUCKET_URL}/users/${user.picture}`} alt=""/>
                                          : <img src={userIcon} alt=""/>
                                      }
                                    </i>
                                    <i>{user.name}</i>
                                  </span>
                                  <ul>
                                    {
                                      user.role > 20
                                        && <li><NavLink to={sitemap.getStudio} activeClassName="active" onClick={this.handleHideDrawer}>Get A Studio</NavLink></li>
                                    }
                                    {
                                      user.role > 10
                                      && user.role < 21
                                      && (
                                        <li><NavLink to={sitemap.uploadArt} activeClassName="active" onClick={this.handleHideDrawer}>Upload Art</NavLink></li>
                                      )
                                    }
                                    <li>
                                      {
                                        user.role > 10 && user.role < 21
                                          ? <NavLink to={generatePath(sitemap.artist, { slug: user.slug })} activeClassName="active" onClick={this.handleHideDrawer}>Profile</NavLink>
                                          : <NavLink to={sitemap.profile} activeClassName="active" onClick={this.handleHideDrawer}>Profile</NavLink>
                                      }
                                    </li>
                                    <li>
                                      <NavLink to={sitemap.editInformation} activeClassName="active" onClick={this.handleHideDrawer}>
                                        {
                                          user.updated === 'false'
                                            ? 'Update Profile'
                                            : 'Edit Information'
                                        }
                                      </NavLink>
                                    </li>
                                    <li><button onClick={this.logOut}>Log Out</button></li>
                                  </ul>
                                </li>
                              </>
                            )
                        }
                      </li>
                    </>
                  )
                }
                {/* <li className={languageClass}>
                  <span onClick={this.handleShowLanguage}>English<DownIcon/></span>
                  <ul>
                    <li><span>Nepali</span></li>
                    <li><span>Chineese</span></li>
                  </ul>
                </li> */}
              </ul>
            </div>
          </div>
        </SwipeableDrawer>
      </header>
    );
  }
}

MobileHeader.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  isUserRefreshInProgress: PropTypes.bool.isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired
};

MobileHeader.defaultProps = {
  user: null
};

export default withRouter(connect(mapStateToProps)(MobileHeader));
