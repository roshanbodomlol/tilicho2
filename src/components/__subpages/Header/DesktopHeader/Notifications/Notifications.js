import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import NotificationIcon from '@material-ui/icons/NotificationImportant';
import Badge from '@material-ui/core/Badge';
import moment from 'moment';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import sitemap from '../../../../../routing/siteMap';
import { getWithAuth, postAuth } from '../../../../../services/generalApiServices';
import Loading from '../../../../Loading';
import SocketContext from '../../../../../socket';
import { CONFIG } from '../../../../../constants';
import NotificationSVG from '../../../../../assets/icons/Notification.svg';
import './Notifications.scss';

const mapStateToProps = ({ user }) => ({
  user
});

class Notifications extends Component {
  state = {
    loading: true,
    notifications: [],
    newNotification: false
  };

  componentDidMount() {
    this.connect();
    this.checkUnreadNotification();
  }

  componentDidUpdate() {
    this.connect();
  }

  connect = () => {
    const { socket } = this.props;
    if (socket) {
      socket.on('newNotification', () => {
        this.setState({ newNotification: true });
        this.getNotifications();
        // this.checkUnreadNotification();
        // playSound('ping');
      });
    }
  }

  checkUnreadNotification = () => {
    const { user } = this.props;
    let unseenNotifications = false;
    if (user) {
      _.each(user.notifications, (notification) => {
        if (notification.read === 'no') {
          unseenNotifications = true;
        }
      });
    }
    if (unseenNotifications) {
      this.setState({ newNotification: true });
    }
  }

  getNotifications = () => {
    getWithAuth(
      CONFIG.GET_NOTIFICATIONS,
      (notifications) => {
        const sortedNotifications = notifications.sort((notificationA, notificationB) => {
          return moment(notificationA.created).unix() < moment(notificationB.created).unix() ? 1 : -1;
        });
        sortedNotifications.splice(CONFIG.NOTIFICATION_LIMIT);
        this.setState({
          loading: false,
          notifications: sortedNotifications
        });
      },
      (error) => {
        this.setState({
          loading: false
        });
        console.error(error);
      }
    );
  }

  makeNotiificationsSeen = () => {
    const { notifications } = this.state;
    let needToUpdate = false;
    _.each(notifications, (notification) => {
      if (notification.read === 'no') needToUpdate = true;
    });
    if (needToUpdate) {
      postAuth(
        CONFIG.MAKE_NOTIFICATIONS_SEEN,
        () => {
          this.setState({ newNotification: false });
        },
        (e) => {
          console.error(e);
        }
      );
    }
  }


  render() {
    const { loading, notifications, newNotification } = this.state;
    const { footer } = this.props;

    let notificationList = [];
    if (!_.isEmpty(notifications)) {
      notificationList = notifications.map((notification) => {
        return (
          <div key={`notification-key-${notification._id}`} className={`notification-status dropDownItem -${notification.read}`}>
            <Link to={notification.link}>
              {
                `${notification.sender && notification.sender.name} ${notification.message}`
              }
              <p>{moment(notification.created).fromNow()}</p>
            </Link>
          </div>
        );
      });
    }

    if (footer) {
      return (
        <div className="nav-icon">
          <NavLink to={sitemap.notifications} onClick={this.makeNotiificationsSeen}>
            <Badge color="primary" invisible={!newNotification} variant="dot">
              <img src={NotificationSVG} alt=""/>
            </Badge>
          </NavLink>
        </div>
      );
    }
    return (
      <>
        <NavLink
          to={sitemap.notifications}
          activeClassName="active"
          onMouseOver={this.getNotifications}
          onMouseLeave={this.makeNotiificationsSeen}
        >
          {
            newNotification
              ? <NotificationIcon style={{ fill: '#984B48' }}/>
              : <NotificationIcon/>
          }
        </NavLink>

        <div className="dropDownContainer notification-list">
          <div className="inner">
            {
              loading
                ? (
                  <div className="dropDownItem typeLoading" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Loading color="red"/>
                  </div>
                )
                : (
                  <>
                    {
                      notifications.length > 0
                        ? (
                          <>
                            {notificationList}
                          </>
                        )
                        : (
                          <div className="dropDownItem">
                            <span>{'You don\'t have any notifications'}</span>
                          </div>
                        )
                    }
                  </>
                )
            }
          </div>
          <div className="seeMore">
            <Link to={sitemap.notifications}>See More</Link>
          </div>
        </div>
      </>
    );
  }
}

const SocketedNotifications = props => (
  <SocketContext.Consumer>
    {
      socket => <Notifications {...props} socket={socket}/>
    }
  </SocketContext.Consumer>
);

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  socket: PropTypes.objectOf(PropTypes.any).isRequired,
  footer: PropTypes.bool
};

Notifications.defaultProps = {
  footer: false
};

export default connect(mapStateToProps)(SocketedNotifications);
