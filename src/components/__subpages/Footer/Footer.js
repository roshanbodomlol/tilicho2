import React from 'react';
import { NavLink } from 'react-router-dom';
import { Breakpoint } from 'react-socks';

import Logo from '../../Logo/Logo';
import Styles from './Footer.module.scss';

const Footer = () => (
  <Breakpoint m>
    <footer id={Styles.footerWrapper} className="footerWrapper">
      <div className="customContainer headerLayout">
        <div className={Styles.footerBlock}>
          <div className={Styles.footerItem}>
            <Logo/>
          </div>
          <div className={Styles.footerItem}>
            <ul>
              <li><NavLink to="/404">Terms of Use</NavLink></li>
              <li><NavLink to="/404">Privacy</NavLink></li>
              <li><NavLink to="/404">Policy</NavLink></li>
              <li><NavLink to="/404">Security</NavLink></li>
              <li><NavLink to="/404">Conditions of Sale</NavLink></li>
            </ul>
            <ul>
              <li>© 2019 Tilicho Kala</li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </Breakpoint>
);

export default Footer;
