import React, { Component } from 'react';
import AddImageIcon from '@material-ui/icons/AddPhotoAlternate';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import CropImage from '../CropImage';
import './ImageUploader.scss';

const styles = {
  iconStyles: {
    color: '#984B48'
  },
  closeIconStyles: {
    color: '#fff'
  }
};


class CoverUploader extends Component {
  inputFileRef = React.createRef();

  handleUploadClick = () => {
    if (this.inputFileRef) {
      this.inputFileRef.current.click();
    }
  }

  render() {
    const { classes, imageFile, width, height, setCroppedImage, onImageChange } = this.props;
    return (
      <div className="image-uploader-wrap">
        <input
          type="file"
          style={{ display: 'none' }}
          ref={this.inputFileRef}
          onChange={onImageChange}
        />
        {
          imageFile
            ? (
              <CropImage
                setCroppedImage={setCroppedImage}
                image={imageFile}
                width={width}
                height={height}
              />
            )
            : (
              <div
                role="button"
                tabIndex="-1"
                className="image-uploader"
                onClick={this.handleUploadClick}
              >
                <AddImageIcon className={classes.iconStyles}/>
                <span>Click to add image</span>
              </div>
            )
        }
      </div>
    );
  }
}

CoverUploader.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  setCroppedImage: PropTypes.func.isRequired,
  onImageChange: PropTypes.func.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  imageFile: PropTypes.instanceOf(new FileReader())
};

CoverUploader.defaultProps = {
  imageFile: null
};

export default withStyles(styles)(CoverUploader);
