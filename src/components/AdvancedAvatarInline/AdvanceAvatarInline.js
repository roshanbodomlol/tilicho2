import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router';

import sitemap from '../../routing/siteMap';
import FollowArtist from '../FollowArtist';
import Avatar from '../Avatar';
import CommonButton from '../Button';
import './advanceAvatarInline.scss';

/**
 * Display Avatar with more info
 * @visibleName Advanced Avatar
 */
const AdvancedAvatarInline = ({ artist, name, imgPath, followers, works, intl }) => {
  const detailsArray = [];
  if (followers) detailsArray.push(intl.formatMessage({ id: 'ui.artist.followers' }, { count: followers }));
  if (works) detailsArray.push(intl.formatMessage({ id: 'ui.artist.works' }, { count: works }));
  const detailsString = detailsArray.join(' / ');
  console.log('asd', imgPath);
  return (
    <div className="advanceAvatarInline">
      {
        imgPath && (
          <Avatar imgPath={imgPath} alt={name}/>
        )
      }
      <div className="infoContainer">
        <div className="name">
          <h4>{name}</h4>
        </div>
        <div className="details">
          <h6>{detailsString}</h6>
        </div>
        <div className="action">
          <ul>
            <li>
              <FollowArtist artist={artist}/>
            </li>
            <li>
              <Link to={generatePath(sitemap.artist, { slug: artist.slug })}>
                <CommonButton size="medium" mode="specialBlack">View Profile</CommonButton>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

AdvancedAvatarInline.propTypes = {
  artist: PropTypes.objectOf(PropTypes.any).isRequired,
  imgPath: PropTypes.string,
  followers: PropTypes.number,
  works: PropTypes.number,
  name: PropTypes.string.isRequired,
  intl: intlShape.isRequired
};

AdvancedAvatarInline.defaultProps = {
  imgPath: null,
  followers: null,
  works: null
};

export default injectIntl(AdvancedAvatarInline);
