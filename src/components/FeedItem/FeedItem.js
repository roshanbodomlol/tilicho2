import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { connect } from 'react-redux';
import { generatePath } from 'react-router';
import { Link } from 'react-router-dom';

import { CONFIG } from '../../constants';
import Avatar from '../Avatar';
import Comment from '../Comment';
import ButtonLike from '../ButtonLike';
import DropdownMenu from '../DropdownMenu';
import EditFeed from './EditFeed';
import sitemap from '../../routing/siteMap';
import store from '../../redux';
import { showDialog, hideDialog } from '../../redux/actions/dialog.actions';
import { showGlobalSnack } from '../../redux/actions/snack.actions';
import { postAuthMultipart } from '../../services/generalApiServices';
import { nl2br } from '../../utils/stringUtils';
import './FeedItem.scss';

const mapStateToProps = ({ user }) => ({ user });

class FeedItem extends Component {
  state = {
    showComments: false,
    editDialogVisible: false
  };

  deletePost = () => {
    const { post, refresh } = this.props;
    store.dispatch(hideDialog());
    store.dispatch(showGlobalSnack('normal', 'Deleting post...', 10000));
    postAuthMultipart(
      generatePath(CONFIG.DELETE_POST, { id: post.id }),
      () => {
        store.dispatch(showGlobalSnack('normal', '', 3000));
        store.dispatch(showGlobalSnack('normal', 'Post has been deleted.', 2000));
        refresh();
      },
      (e) => {
        console.log(e);
        store.dispatch(showGlobalSnack('error', 'An error occurred. Please try again', 2000));
      }
    );
  }

  toggleComments = (flag = false) => {
    this.setState({ showComments: flag });
  }

  showEditDialog = () => {
    this.setState({
      editDialogVisible: true
    });
  }

  hideEditDialog = () => {
    this.setState({
      editDialogVisible: false
    });
  }

  handleDelete = () => {
    store.dispatch(showDialog('Delete Post', 'Are you sure you want to delete this post?', this.deletePost, 'Delete', 'Cancel'));
  }

  render() {
    const { post, refresh, user } = this.props;
    const { showComments, editDialogVisible } = this.state;
    const dropdownActions = [
      {
        id: `post-edit-action-${post.id}`,
        name: 'Edit',
        onClick: this.showEditDialog
      },
      {
        id: `post-delete-action-${post.id}`,
        name: 'Delete',
        onClick: this.handleDelete
      }
    ];
    return (
      <div className="feedItem">
        {
          editDialogVisible && (
            <EditFeed post={post} handleClose={this.hideEditDialog} refresh={refresh}/>
          )
        }
        <div className="feedTitle">
          <Avatar imgPath={`${CONFIG.BUCKET_URL}/users/${post.author.picture}`}/>
          <div className="authorName">
            {
              post.author.role > 10 && post.author.role < 21 && !!post.author.slug
                ? <h4><Link to={generatePath(sitemap.artist, { slug: post.author.slug })}>{post.author.name}</Link></h4>
                : <h4>{post.author.name}</h4>
            }
            <p className="coSmallText">{moment(post.created).fromNow()}</p>
          </div>
          {
            user && user.id === post.author.id
              && (
                <DropdownMenu actions={dropdownActions}/>
              )
          }
        </div>
        <div className="feedContent">
          {
            post.picture
              && (
                <div className="imageContainer">
                  <img src={`${CONFIG.BUCKET_URL}/posts/${post.picture}`} alt=""/>
                </div>
              )
          }
          <div className="textContainer">
            <p>
              {nl2br(post.post)}
            </p>
          </div>
          <div className="commonLike">
            <div className="inner">
              <div className="icon">
                <FavoriteIcon/><span>{post.likes.length}</span>
              </div>
              <div className="likebtn">
                <ButtonLike item={post} refresh={refresh}/>
              </div>
            </div>
          </div>
          <div className="comments-section">
            {
              showComments
                ? (
                  <div
                    className="view-comments"
                    onClick={() => this.toggleComments()}
                    role="button"
                    tabIndex="-1"
                  >
                    Hide Comments
                  </div>
                )
                : (
                  <div
                    className="view-comments"
                    onClick={() => this.toggleComments(true)}
                    role="button"
                    tabIndex="-1"
                  >
                    {
                      post.comments && post.comments.length > 0
                        ? `View Comments (${post.comments.length})`
                        : 'Write a comment'
                    }
                  </div>
                )
            }
            {
              showComments && <Comment item={post}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

FeedItem.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  refresh: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(FeedItem);
