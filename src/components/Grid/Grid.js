import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './Grid.module.scss';

class Grid extends Component {
  constructor() {
    super();
    this.state = {
      cols: 0
    };
    this.gridRef = React.createRef();
  }

  componentDidMount() {
    console.log(this.gridRef.current.clientWidth);
    const { columnWidth } = this.props;
    const containerWidth = this.gridRef.current.clientWidth;
    const n = containerWidth / columnWidth + 1;
    console.log(Math.floor(n));
    this.setState({ cols: n });
  }

  render() {
    let { cols } = this.state;
    cols = Math.floor(cols);
    const { gutter } = this.props;
    const columns = [];
    for (let i = 0; i < cols; i++) {
      console.log(i, cols);
      const col = i === (cols - 1) ? <div className={styles.col}/> : <div style={{ marginRight: gutter }} className={styles.col}/>
      columns.push(col);
    }
    return (
      <div className={styles.grid} ref={this.gridRef}>
        <h1>Hey</h1>
        <div className={styles.gridRow}>
          {columns}
        </div>
      </div>
    );
  }
}

Grid.propTypes = {
  // childern: PropTypes.node.isRequired,
  columnWidth: PropTypes.number.isRequired,
  gutter: PropTypes.number
};

Grid.defaultProps = {
  gutter: 10
};

export default Grid;
