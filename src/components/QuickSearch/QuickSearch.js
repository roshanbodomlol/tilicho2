import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Link } from 'react-router-dom';
import { each } from 'lodash';
import PropTypes from 'prop-types';

import Loading from '../Loading';
import sitemap from '../../routing/siteMap';
import { CONFIG } from '../../constants';
import { get } from '../../services/generalApiServices';
import { getThumbnail } from '../../utils/mediaUtils';
import './QuickSearch.scss';

class QuickSearch extends Component {
  state = {
    searchValue: '',
    showingResults: false,
    products: [],
    artists: [],
    searching: false
  };

  handleInputChange = (e) => {
    this.setState({ searchValue: e.target.value });
  }

  handleSearchSubmit = (e) => {
    e.preventDefault();
    this.search();
  }

  search = () => {
    const { searchValue } = this.state;
    const { mobile } = this.props;

    if (searchValue !== '') {
      if (mobile) {
        window.location.href = generatePath(sitemap.searchPage, { term: searchValue })
      } else {
        this.setState({ searching: true, showingResults: true });
        // event listener
        document.addEventListener('click', this.deactivateSearch);
        get(
          generatePath(CONFIG.SEARCH, { term: searchValue }),
          data => this.setState({ products: data.products, artists: data.artists, searching: false }),
          () => this.setState({ searching: false, showingResults: false })
        );
      }
    }
  }

  deactivateSearch = (e) => {
    let externalClick = true;
    each(e.path, (elem) => {
      if (elem.id === 'search') {
        externalClick = false;
      }
    });
    if (externalClick) {
      document.removeEventListener('click', this.deactivateSearch);
      this.setState({ showingResults: false, searching: false, products: [], artists: [] });
    }
  }

  render() {
    const { searchValue, products, artists, showingResults, searching } = this.state;

    const productsList = products.map((product) => {
      const thumbnail = getThumbnail(product.image, 'thumbnail');
      return (
        <div className="search-result product" key={`search-result-${product._id}`}>
          <Link to={generatePath(sitemap.artWork, { slug: product.slug })}>
            <img src={thumbnail.url} alt=""/>
            <div className="info">
              <div className="name">{product.name}</div>
              <div className="author">{product.author.name}</div>
            </div>
          </Link>
        </div>
      );
    });
    const artistsList = artists.map((artist) => {
      return (
        <div className="search-result artist" key={`search-result-${artist._id}`}>
          <Link to={generatePath(sitemap.artist, { slug: artist.slug })}>
            <img src={`${CONFIG.BUCKET_URL}/users/${artist.picture}`} alt=""/>
            <div className="info">
              <div className="name">{artist.name}</div>
            </div>
          </Link>
        </div>
      );
    });

    return (
      <div className="searchContainer">
        <form onSubmit={this.handleSearchSubmit}>
          <input
            type="text"
            placeholder="Type to search"
            value={searchValue}
            onChange={this.handleInputChange}
          />
          <button type="submit" style={{ display: 'none' }}/>
        </form>
        {
          showingResults && (
            <div className="results">
              {
                searching
                  ? <Loading/>
                  : (
                    <>
                      {
                        products.length < 1 && artists < 1 && (
                          <div className="none">No results found</div>
                        )
                      }
                      {productsList}
                      {artistsList}
                    </>
                  )
              }
            </div>
          )
        }
      </div>
    );
  }
}

QuickSearch.propTypes = {
  mobile: PropTypes.bool
};

QuickSearch.defaultProps = {
  mobile: false
};

export default QuickSearch;
