import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { injectIntl, intlShape } from 'react-intl';

import styles from './AdvancedAvatar.scss';
import Avatar from '../Avatar';

/**
 * Display Avatar with more info
 * @visibleName Advanced Avatar
 */
const AdvancedAvatar = ({ className, width, imgPath, followers, works, name, intl }) => {
  const detailsArray = [];
  if (followers) detailsArray.push(intl.formatMessage({ id: 'ui.artist.followers' }, { count: followers }));
  if (works) detailsArray.push(intl.formatMessage({ id: 'ui.artist.works' }, { count: works }));
  const detailsString = detailsArray.join(' / ');
  return (
    <div className={classnames(styles.advancedAvatar, className)}>
      <Avatar width={width} imgPath={imgPath} alt={name}/>
      <div className="info">
        <div className="name">
          <h4>{name}</h4>
        </div>
        <div className="details">
          <h6>{detailsString}</h6>
        </div>
      </div>
    </div>
  );
};

AdvancedAvatar.propTypes = {
  className: PropTypes.string,
  width: PropTypes.number,
  imgPath: PropTypes.string,
  followers: PropTypes.number,
  works: PropTypes.number,
  name: PropTypes.string.isRequired,
  intl: intlShape.isRequired
};

AdvancedAvatar.defaultProps = {
  className: '',
  width: 150,
  imgPath: null,
  followers: null,
  works: null
};

export default injectIntl(AdvancedAvatar);
