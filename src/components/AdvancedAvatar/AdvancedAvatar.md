```js
<div style={{
  display: 'flex',
  justifyContent: 'space-around'
}}>
<AdvancedAvatar
  width={200}
  imgPath="http://storage.googleapis.com/tilichobucket/users/c5b75cbf-8bbe-40c8-8e95-ded05751bed6-1550139827313.jpeg"
  name="Velvet Buzzsaw"
  followers={250}
  works={1}
/>
<AdvancedAvatar
  width={150}
  imgPath="http://storage.googleapis.com/tilichobucket/users/c5b75cbf-8bbe-40c8-8e95-ded05751bed6-1550139827313.jpeg"
  name="Jane Doe"
  works={3}
/>
</div>
```
