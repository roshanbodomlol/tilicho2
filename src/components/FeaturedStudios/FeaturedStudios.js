import React, { Component } from 'react';
import Slider from 'react-slick';

import { getFeatured } from '../../services/studioServices';
import Loading from '../Loading';
import { getThumbnail } from '../../utils/mediaUtils';
import { CONFIG } from '../../constants';
import AdvanceAvatarInline from '../AdvancedAvatarInline';
import './FeaturedStudios.scss';

class FeaturedStudios extends Component {
  state = {
    studios: [],
    isLoading: true
  };

  componentDidMount() {
    getFeatured()
      .then(studios => this.setState({ studios, isLoading: false }, this.getMedia))
      .catch((error) => {
        console.log('Error occurred while trying to fetch featured studios', error);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { studios, isLoading } = this.state;

    const sliderItems = studios.map((studio) => {
      const thumbnail = studio.product && getThumbnail(studio.product.image, 'productImage');

      return (
        <div className="featured-studio">
          <div className="_inner">
            {
              thumbnail && (
                <div className="studio-product-image">
                  <img src={`${CONFIG.BUCKET_URL}/${studio.product.image.set.slug}/${thumbnail.fileName}`} alt=""/>
                </div>
              )
            }
            <div className="studio-info">
              <AdvanceAvatarInline
                artist={studio}
                followers={studio.followsCount}
                works={studio.productsCount}
                name={studio.name}
                imgPath={false}
              />
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="featured-studios">
        <div className="customContainer mediumWidthLayout">
          {
            isLoading
              ? <Loading color="red"/>
              : (
                <div className="featured-studio-slider">
                  <Slider
                    fade
                    dots
                    autoplay
                    arrows={false}
                  >
                    {sliderItems}
                  </Slider>
                </div>
              )
          }
        </div>
      </div>
    );
  }
}

export default FeaturedStudios;
