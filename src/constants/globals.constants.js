export default {
  DEFAULT_SITE_LANGUAGE: 'EN',
  EXIF_ORIENTATION_TO_TRANSFORM: {
    1: 'rotateZ(0deg)',
    3: 'rotateZ(180deg)',
    6: 'rotateZ(90deg)',
    8: 'rotateZ(270deg)'
  }
};
