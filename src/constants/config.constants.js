const BACKEND = 'https://anmaritamang.com';
// const BACKEND = 'http://localhost';
const API_URL = `${BACKEND}/api`;
const AUTH_URL = `${BACKEND}/auth`;

export default {
  DEFAULT_SITE_LANGUAGE: 'en',
  AUTH_URL: `${BACKEND}/auth`,
  API_URL: `${BACKEND}/api`,
  SOCKET_URL: BACKEND,
  // SOCKET_URL: 'http://localhost',
  BUCKET_URL: 'https://storage.googleapis.com/tilichobucket',
  GALLERY_PAGE_LIMIT: 15,
  ARTISTS_PAGE_LIMIT: 10,
  ARTISTS_WORK_LIMIT: 12,
  FEED_LIMIT: 10,
  FEED_COMMENTS_LIMIT: 5,
  HOMEPAGE: 'https://tulikaa.com',
  CURRENCY: 'NPR ',
  AUTH_TOKEN: 'tulitoken',
  POST_ATTACHMENT_MB: 5,
  NOTIFICATION_LIMIT: 15,
  FEED_TOP_STUDIOS_LIMIT: 5,
  UPLOAD_ART_IMAGES_LIMIT: 6,
  LANG_COOKIE_NAME: '__tulika__lang__',
  CART_COOKIE_NAME: '__tulika__cart__',
  //
  // API ENDPOINTS
  GET_TOP_ARTISTS: `${API_URL}/artists/top/all`,
  GET_ALL_ARTISTS: `${API_URL}/users/artists`,
  GET_POSTS_ALL: `${API_URL}/v2/posts`,
  GET_USER_BY_SLUG: `${API_URL}/user/getbyslug/:slug`,
  GET_USER_BY_ID: `${API_URL}/user/get/:id`,
  GET_ARTIST_BY_SLUG: `${API_URL}/artist/:slug`,
  GET_POSTS_BY_ARTIST: `${API_URL}/post/getbyartist/:id/:total`,
  GET_ARTISTS_SORTED: `${API_URL}/artists/sortable`,
  GET_ARTWORK_BY_SLUG: `${API_URL}/product/:slug`,
  GET_ARTWORK_BY_SIMILAR: `${API_URL}/products/similar/:slug`,
  GET_PRODUCTS_BY_USERID: `${API_URL}/user/gallery/:id`,
  GET_PRODUCTS_WITH_IMAGES: `${API_URL}/artist/:artistId/productswithimages`,
  GET_PRODUCTS_BY_CAT: `${API_URL}/v2/products/category/:cat`,
  GET_PRODUCTS_BY_FOLLOWED: `${API_URL}/products/followed`,
  GET_CATEGORIES: `${API_URL}/categories`,
  GET_CATEGORIES_SORTED: `${API_URL}/categories/sorted`,
  GET_NOTIFICATIONS: `${API_URL}/notifications/all`, // auth
  MAKE_NOTIFICATIONS_SEEN: `${API_URL}/notifications/makeseen`, // post (auth),
  GET_POST_COMMENTS: `${API_URL}/v2/post/comments/:postId/:total`,
  MAKE_POST_COMMENT: `${API_URL}/v2/post/comments/new/:postId`,
  CLEAR_NOTIFICATIONS: `${API_URL}/notifications/clear`, // post auth
  UPLOAD_ART: `${API_URL}/user/upload`, // post auth
  UPDATE_ART: `${API_URL}/user/edit/:id`, // post auth
  DELETE_POST: `${API_URL}/post/delete/:id`, // post auth
  UPDATE_POST: `${API_URL}/post/update/:id`, // post auth multipart
  UPDATE_USER_INFO: `${API_URL}/user/update`, // post auth multipart
  GET_STUDIO: `${API_URL}/users/become-artist`, // post auth multipart
  VALIDATE_CODE: `${API_URL}/users/validatecode`, // post auth
  LIKE_PRODUCT: `${API_URL}/like/product/:productId`,
  FOLLOW_ARTIST: `${API_URL}/secure/followartist`,
  GET_PRODUCTS_BY_IDS: `${API_URL}/products/getbyids`,
  GET_FEATURED_PRODUCTS: `${API_URL}/products/getfeatured`,
  GET_FEATURED_STUDIOS: `${API_URL}/studios/featured`,
  GET_MEDIA_BY_ID: `${API_URL}/mediafile/:id`,
  DELETE_ART: `${API_URL}/user/del/:id`,

  // STUDIOS
  GET_STUDIOS_TO_FOLLOW: `${API_URL}/studios/tofollow`,

  // MESSAGES
  GET_MESSAGES: `${API_URL}/messages/getbyuserslug/:to`,
  GET_ACTIVE_MESSAGES: `${API_URL}/messages/active`,
  GET_CONVERSATIONS: `${API_URL}/messages/all`,
  CREATE_MESSAGE: `${API_URL}/messages/new`, // post auth
  MAKE_CONVERSATION_SEEN: `${API_URL}/messages/makeseen/:id`, // post auth
  INITIATE_FOLLOWS: `${API_URL}/user/initiatefollows`, // post auth
  CHECK_FOLLOW_REQUEST: `${API_URL}/user/checkfollow`, // post auth
  APPROVE_FOLLOW: `${API_URL}/user/approvefollow`,
  CANCEL_FOLLOW_REQUEST: `${API_URL}/user/cancelrequest`,

  // VERIFICATION
  VERIFY_CODE: `${AUTH_URL}/verifyemail/:code`, // post
  RESEND_CODE: `${AUTH_URL}/resendverificationemail`, // post

  RESET: `${AUTH_URL}/resetpassword`,

  // SEARCH
  SEARCH: `${API_URL}/search/:term`,

  // ESEWA
  // ESEWA_URL: 'https://uat.esewa.com.np/',
  // PLACE_ORDER: `${API_URL}/users/payment/order` // POST AUTH

  // FB LOGIN
  FB_VERIFY_TOKEN: `${AUTH_URL}/social/fb/checktoken`,

  // GOOGLE LOGIN
  GOOGLE_VERIFY_TOKEN: `${AUTH_URL}/social/google/checktoken`,

  // EXHIBITION
  LIST_EXHIBITIONS: `${API_URL}/exhibitions`,
  GET_EXHIBITION: `${API_URL}/exhibitions/:id`,

  // SEND EMAIL AUTH
  SEND_EMAIL: `${API_URL}/sendemail`
};
