import { createMuiTheme } from '@material-ui/core/styles';

const palette = {
  primary: {
    main: '#984B48'
  },
  secondary: {
    main: '#212121'
  },
  special: {
    main: '#ffffff'
  }
};

export default createMuiTheme({
  palette,
  shadows: ['none'],
  typography: {
    useNextVariants: true
  }
});
