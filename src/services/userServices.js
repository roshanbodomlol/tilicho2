import Cookie from 'js-cookie';
import axios from 'axios';

import { CONFIG } from '../constants';

export const refreshUser = (begin, success, fail) => {
  const token = Cookie.get(CONFIG.AUTH_TOKEN);
  if (token) {
    begin();
    axios({
      url: `${CONFIG.AUTH_URL}/authenticate`,
      method: 'POST',
      headers: {
        Authorization: `JWT ${token}`
      }
    })
      .then((response) => {
        if (response.data.status === 'success') {
          success(response.data.user, token);
        } else throw new Error(response.data.message);
      })
      .catch((e) => {
        fail(e);
      });
  } else {
    fail('no token');
  }
};
