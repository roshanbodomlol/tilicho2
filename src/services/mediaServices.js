import axios from 'axios';
import { generatePath } from 'react-router';

import { CONFIG } from '../constants';

export const getMediaFromID = () => {
  return new Promise((resolve, reject) => {
    axios({
      url: generatePath(CONFIG.GET_MEDIA_BY_ID),
      method: 'GET'
    })
      .then((media) => {
        resolve(media);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
