import axios from 'axios';
import Cookie from 'js-cookie';
import { generatePath } from 'react-router';

import { CONFIG } from '../constants';
import store from '../redux';
import { showGlobalSnack } from '../redux/actions/snack.actions';

export const list = () => new Promise((resolve, reject) => {
  axios({
    url: CONFIG.LIST_EXHIBITIONS,
    method: 'GET'
  })
    .then((response) => {
      if (response.data.status === 'success') {
        resolve(response.data.message);
      } else throw new Error(response.data.message);
    })
    .catch((e) => {
      reject(e);
    });
});

export const get = id => new Promise((resolve, reject) => {
  axios({
    url: generatePath(CONFIG.GET_EXHIBITION, { id }),
    method: 'GET'
  })
    .then((response) => {
      if (response.data.status === 'success') {
        resolve(response.data.message);
      } else throw new Error(response.data.message);
    })
    .catch((e) => {
      reject(e);
    });
});
