import axios from 'axios';

import { CONFIG } from '../constants';

export const getAllPosts = (params, success, fail) => {
  const apiUrl = CONFIG.GET_POSTS_ALL;
  axios({
    url: apiUrl,
    method: 'GET',
    params
  })
    .then((response) => {
      if (response.data.status === 'success') {
        success({ posts: response.data.message, total: response.data.total });
      } else throw new Error(response.data.message);
    })
    .catch((e) => {
      fail(e);
    });
};
