import { SET_LOGIN_BACK } from '../constants/action-types';

const initState = {
  loginBack: null
};

export default (state = initState, action) => {
  switch (action.type) {
    case SET_LOGIN_BACK:
      return {
        loginBack: action.payload
      };
    default:
      return state;
  }
};
