import Cookies from 'js-cookie';

import { CONFIG } from '../../constants';
import {
  ADD_TO_CART,
  REMOVE_CART_ITEM
} from '../constants/action-types';

const cartCookie = Cookies.get(CONFIG.CART_COOKIE_NAME);

const initialState = {
  items: cartCookie ? JSON.parse(cartCookie) : []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      const { items } = state;
      const newItems = [...items];
      newItems.push({ ...action.payload });
      Cookies.set(CONFIG.CART_COOKIE_NAME, JSON.stringify(newItems), { expires: 7, path: '/' });
      return {
        items: newItems
      };
    }
    case REMOVE_CART_ITEM: {
      const { items } = state;
      const newItems = [...items];
      const indexToBeRemoved = newItems.findIndex(item => item.productId === action.payload.productId);
      if (indexToBeRemoved > -1) {
        newItems.splice(indexToBeRemoved, 1);
      }
      Cookies.set(CONFIG.CART_COOKIE_NAME, JSON.stringify(newItems), { expires: 7, path: '/' });
      return {
        items: newItems
      };
    }
    default:
      return state;
  }
};
