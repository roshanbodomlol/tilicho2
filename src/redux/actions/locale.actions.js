import { SET_LOCALE } from '../constants/action-types';

export const setLang = lang => (
  { type: SET_LOCALE, lang }
);

export const none = () => {};
