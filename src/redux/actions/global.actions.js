import { SET_LOGIN_BACK } from '../constants/action-types';

export const setLoginBack = path => ({
  type: SET_LOGIN_BACK,
  payload: path
});
