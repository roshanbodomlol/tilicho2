import {
  ADD_TO_CART,
  UPDATE_CART_ITEM,
  REMOVE_CART_ITEM
} from '../constants/action-types';

export const addToCart = (productId, quantity) => ({
  type: ADD_TO_CART,
  payload: {
    productId,
    quantity
  }
});

export const updateCartItem = (productId, quantity) => ({
  type: UPDATE_CART_ITEM,
  payload: {
    productId,
    quantity
  }
});

export const removeCartItem = productId => ({
  type: REMOVE_CART_ITEM,
  payload: {
    productId
  }
});
