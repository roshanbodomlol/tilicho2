import { SHOW_DIALOG, HIDE_DIALOG } from '../constants/action-types';

export const showDialog = (title, message, confirm, yesLabel = 'Yes', noLabel = 'No') => ({
  type: SHOW_DIALOG,
  payload: {
    title,
    message,
    confirm,
    yesLabel,
    noLabel
  }
});

export const hideDialog = () => (
  {
    type: HIDE_DIALOG
  }
);
